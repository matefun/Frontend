# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Hay que asegurarse que las variables SERVER y GHCI_URL coinciden con la ruta
# en que se despliegan los servicios. Para ello, editar el archivo
#    /Frontend\ Angular\ 4/src/app/shared/config.ts
#
# Los servicios y la aplicacion se despliegan en la ruta configurada en el tag
# <context-root> del archivo:
#   /Frontend/Servidor JEE/WebContent/WEB-INF/jboss-web.xml
#
# Hay que asegurarse que el --base-href coincida con la ruta en la que se
# despliega la aplicacion. En otras palabras, el --base-href y el contenido
# del tag <context-root> DEBEN COINCIDIR.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


cd Frontend\ Angular\ 4/
npx ng build --configuration production --base-href /proyectos/matefun/
cd ..
cp -R Frontend\ Angular\ 4/dist/* Servidor\ JEE/WebContent/
cd Servidor\ JEE/
mvn package
cd ..
