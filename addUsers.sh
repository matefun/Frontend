#! /bin/bash


# csv: user,nombre,apellido,pass,rol(docente,alumno)
# todas las filas tienen que terminar con enter
INPUT='users.csv'
#users.csv

SERVER=https://www.fing.edu.uy/proyectos/matefun/servicios
# http://localhost:8080/servicios 

# parametros del script: usuario y pass
read USER # =  $1
stty -echo
read PASS # =  $2
stty echo

echo "//$USER//$PASS//"

token=$(curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"cedula":"'$USER'","password":"'$PASS'"}' \
  "$SERVER/login" | \
    python3 -c "import sys, json; print(json.load(sys.stdin)['token'])") 

while IFS=, read -r user name surname code role; do
    echo "user: $user"
    hcode=$(echo -n "$code" | sha1sum | awk '{print $1}')
    echo "pass: $hcode"
    echo "token: $token"
    data='{ "rol": "'$role'","cedula": "'$user'", "nombre": "'$name'","apellido": "'$surname'","password": "'$hcode'","configuracion": { "themeEditor": "dracula","fontSizeEditor": 12,"argumentoI": true,"argumentoF": true}}'
    echo $data
    curl -H "Content-Type: application/json" -H "Authorization: Bearer $token" \
         --request POST \
         --data "$data" \
         "$SERVER/usuario/$role/restore"
    echo "\n"

done < $INPUT

curl -H "Content-Type: application/json" -H "Authorization: Bearer $token" \
  --request POST \
  "$SERVER/login/$USER/logout" 

