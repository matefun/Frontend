import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { TranslateService } from "../../node_modules/@ngx-translate/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent implements OnInit {
  constructor(public router: Router, public translate: TranslateService) {
    this.translate.addLangs(["es", "en"]);
    let currentSession = sessionStorage.getItem("currentUser");
    let language = currentSession ? JSON.parse(currentSession).language : "es";
    if (language) {
      this.translate.setDefaultLang(language);
    } else {
      this.translate.setDefaultLang("es");
    }
  }
  ngOnInit() {
    //this.router.navigate(['/login']);
  }
}
