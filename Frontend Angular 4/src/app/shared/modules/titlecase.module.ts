import { NgModule } from "@angular/core";
import { TitleCasePipe } from "../pipes/titlecase.pipe";

@NgModule({
  imports: [],
  declarations: [TitleCasePipe],
  exports: [TitleCasePipe],
})
export class TitleCaseModule {}
