import { NgModule } from "@angular/core";
import { ClosePopoverDirective } from "./closePopover.directive";

@NgModule({
  imports: [],
  declarations: [ClosePopoverDirective],
  exports: [ClosePopoverDirective],
})
export class DirectivesModule {}
