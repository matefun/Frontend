
// psico
// export const SERVER = 'https://matefun.math.psico.edu.uy';
// export const GHCI_URL = 'wss://matefun.math.psico.edu.uy/endpoint';

// fing
export const SERVER = 'https://www.fing.edu.uy/proyectos/matefun';
export const GHCI_URL = 'wss://www.fing.edu.uy/proyectos/matefun/endpoint';


// local
// export const SERVER = 'http://localhost:8080';
// export const GHCI_URL = 'ws://localhost:8080/endpoint';


