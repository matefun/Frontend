import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { map } from "rxjs/operators";
import { Usuario } from "../objects/usuario";
import { SERVER } from "../config";

@Injectable()
export class AuthenticationService {
  constructor(private http: HttpClient) {}

  login(cedula: string, password: string, language: string) {
    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    let httpOptions = { headers: headers };

    return this.http
      .post(
        SERVER + "/servicios/login",
        JSON.stringify({ cedula: cedula, password: password }),
        httpOptions
      )
      .pipe(
        map((response: any) => {
          response.language = language;
          sessionStorage.setItem("currentUser", JSON.stringify(response));
        })
      );
  }

  getUser(): Usuario {
    return JSON.parse(sessionStorage.getItem("currentUser"));
  }

  getUserConfig() {
    return JSON.parse(sessionStorage.getItem("currentUser")).configuracion;
  }

  getToken() {
    var currentUser = JSON.parse(sessionStorage.getItem("currentUser"));
    return currentUser ? currentUser.token : undefined;
  }

  getLanguage() {
    var currentUser = JSON.parse(sessionStorage.getItem("currentUser"));
    return currentUser ? currentUser.language : undefined;
  }

  setUserConfig(config) {
    var user = JSON.parse(sessionStorage.getItem("currentUser"));
    user.configuracion = config;
    sessionStorage.setItem("currentUser", JSON.stringify(user));
  }

  logout() {
    // remove user from local storage to log user out
    sessionStorage.removeItem("currentUser");
  }
}
