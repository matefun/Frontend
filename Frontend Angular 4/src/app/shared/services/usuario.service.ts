import { throwError as observableThrowError, Observable } from "rxjs";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Configuracion } from "../objects/usuario";
import { TranslateService } from "@ngx-translate/core";
import { AuthenticationService } from "./authentication.service";
import { map, catchError } from "rxjs/operators";

import { SERVER } from "../config";

@Injectable()
export class UsuarioService {
  translateService: any;

  constructor(
    private http: HttpClient,
    private router: Router,
    private authService: AuthenticationService,
    public translate: TranslateService
  ) {
    this.translateService = translate;
  }

  actualizarConfiguracion(cedula: string, config: Configuracion) {
    let headers = new HttpHeaders({
      "Content-Type": "application/json",
      Authorization: "Bearer " + this.authService.getToken(),
    });
    let options = { headers: headers };
    return this.http
      .put(
        SERVER + "/servicios/usuario/" + cedula + "/configuracion",
        config,
        options
      )
      .pipe(
        map((data) => this.extractData(data)),
        catchError(this.handleError)
      );
  }

  private extractData(res: any) {
    let body = res;
    return body || [];
  }

  private handleError(error: any) {
    if (error.status == 401) {
      this.translateService
        .get("i18n.code")
        .subscribe((res) => this.router.navigate(["/" + res + "/login"]));
    }
    let errMsg = error.message
      ? error.message
      : error.status
      ? `${error.status} - ${error.statusText}`
      : "Server error";
    console.error(errMsg); // log to console instead
    return observableThrowError(errMsg);
  }
}
