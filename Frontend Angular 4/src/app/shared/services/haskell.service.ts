import { throwError as observableThrowError, Observable } from "rxjs";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Archivo, Evaluacion } from "../objects/archivo";
import { Grupo } from "../objects/grupo";

import { SERVER } from "../config";
import { TranslateService } from "@ngx-translate/core";
import { AuthenticationService } from "./authentication.service";
import { catchError } from "rxjs/operators";

@Injectable()
export class HaskellService {
  translateService: any;

  /**
   * Creates a new HaskellService with the injected HttpClient.
   * @param {HttpClient} http - The injected HttpClient.
   * @constructor
   */
  constructor(
    private http: HttpClient,
    private router: Router,
    private authService: AuthenticationService,
    public translate: TranslateService
  ) {
    this.translateService = translate;
  }

  private getHeaders() {
    return new HttpHeaders({
      "Content-Type": "application/json",
      Authorization: "Bearer " + this.authService.getToken(),
    });
  }

  getArchivos(cedula: string): Observable<Archivo[]> {
    let headers = this.getHeaders();
    let params: HttpParams = new HttpParams();
    params = params.set("cedula", cedula);
    let httpOptions = { headers: headers, params: params };

    return this.http
      .get<Archivo[]>(SERVER + "/servicios/archivo", httpOptions)
      .pipe(catchError(this.handleError));
  }

  getArchivosCompartidosAlumno(cedula: string): Observable<Archivo[]> {
    let headers = this.getHeaders();
    let params: HttpParams = new HttpParams();
    params = params.set("cedula", cedula);
    params = params.set("compartidos", "true");
    let httpOptions = { headers: headers, params: params };

    return this.http
      .get<Archivo[]>(SERVER + "/servicios/archivo", httpOptions)
      .pipe(catchError(this.handleError));
  }

  crearArchivo(archivo: Archivo): Observable<Archivo> {
    let headers = this.getHeaders();

    let httpOptions = { headers: headers };

    return this.http
      .post<Archivo>(SERVER + "/servicios/archivo", archivo, httpOptions)
      .pipe(catchError(this.handleError));
  }

  editarArchivo(archivoId, archivo: Archivo): Observable<Archivo> {
    let headers = this.getHeaders();
    let httpOptions = { headers: headers };

    return this.http
      .put<Archivo>(
        SERVER + "/servicios/archivo/" + archivoId,
        archivo,
        httpOptions
      )
      .pipe(catchError(this.handleError));
  }

  eliminarArchivo(archivoId): Observable<Response> {
    let headers = this.getHeaders();
    let httpOptions = { headers: headers };

    return this.http
      .delete<Response>(SERVER + "/servicios/archivo/" + archivoId, httpOptions)
      .pipe(catchError(this.handleError));
  }

  getCopiaArchivoCompartidoGrupo(cedula, archivoId): Observable<Archivo> {
    let headers = this.getHeaders();
    let params: HttpParams = new HttpParams();
    params = params.set("cedula", cedula);
    let httpOptions = { headers: headers, params: params };

    return this.http
      .get<Archivo>(
        SERVER + "/servicios/archivo/compartido/" + archivoId,
        httpOptions
      )
      .pipe(catchError(this.handleError));
  }

  compartirArchivoGrupo(grupo, archivoId): Observable<Archivo> {
    let headers = this.getHeaders();
    let httpOptions = { headers: headers };
    var archId = {
      id: archivoId,
    };
    return this.http
      .post<Archivo>(
        SERVER +
          "/servicios/grupo/" +
          grupo.liceoId +
          "/" +
          grupo.anio +
          "/" +
          grupo.grado +
          "/" +
          grupo.grupo +
          "/archivo",
        archId,
        httpOptions
      )
      .pipe(catchError(this.handleError));
  }

  calificarArchivo(archivoId, estado, evaluacion): Observable<Evaluacion> {
    let headers = this.getHeaders();
    let httpOptions = { headers: headers };
    return this.http
      .post<Evaluacion>(
        SERVER +
          "/servicios/archivo/" +
          archivoId +
          "/" +
          estado +
          "/evaluacion",
        evaluacion,
        httpOptions
      )
      .pipe(catchError(this.handleError));
  }

  getGrupos(cedula: string): Observable<Grupo[]> {
    let headers = this.getHeaders();
    let params: HttpParams = new HttpParams();
    params = params.set("cedula", cedula);
    let httpOptions = { headers: headers, params: params };

    return this.http
      .get<Grupo[]>(SERVER + "/servicios/grupo", httpOptions)
      .pipe(catchError(this.handleError));
  }

  /**
   * Handle HTTP error
   */
  private handleError(error: any) {
    if (error.status == 401) {
      this.translateService
        .get("i18n.code")
        .subscribe((res) => this.router.navigate(["/" + res + "/login"]));
    }
    // In a real world app, we might use a remote logging infrastructure
    // We'd also dig deeper into the error to get a better message
    let errMsg = error.message
      ? error.message
      : error.status
      ? `${error.status} - ${error.statusText}`
      : "Server error";
    console.error(errMsg); // log to console instead
    return observableThrowError(errMsg);
  }
}
