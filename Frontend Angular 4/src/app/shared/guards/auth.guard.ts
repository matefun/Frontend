import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";
import { Observable } from "rxjs";
import { TranslateService } from "@ngx-translate/core";

@Injectable()
export class AuthGuard implements CanActivate {
  translateService: any;

  constructor(private router: Router, public translate: TranslateService) {
    this.translateService = translate;
  }

  canActivate(): Observable<boolean> | Promise<boolean> | boolean {
    // El usuario está logeado
    if (sessionStorage.getItem("currentUser")) {
      return true;
    }

    // Se navega al login dependiendo del idioma
    this.translateService
      .get("i18n.code")
      .subscribe((res) => this.router.navigate(["/" + res + "/login"]));

    return false;
  }
}
