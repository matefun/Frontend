import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { AuthGuard } from "./shared/guards/auth.guard";

const routes: Routes = [
  {
    path: "",
    loadChildren: () =>
      import("./layout/layout.module").then((m) => m.LayoutModule),
    canActivate: [AuthGuard],
  },
  {
    path: "login",
    loadChildren: () =>
      import("./login/login.module").then((m) => m.LoginModule),
    data: {
      language:
        navigator.language &&
        (navigator.language.split("-")[0] == "es" ||
          navigator.language.split("-")[0] == "en")
          ? navigator.language.split("-")[0]
          : "es",
    },
  },
  {
    path: "es/login",
    loadChildren: () =>
      import("./login/login.module").then((m) => m.LoginModule),
    data: { language: "es" },
  },
  {
    path: "en/login",
    loadChildren: () =>
      import("./login/login.module").then((m) => m.LoginModule),
    data: { language: "en" },
  },
  {
    path: "not-found",
    loadChildren: () =>
      import("./not-found/not-found.module").then((m) => m.NotFoundModule),
  },
  { path: "**", redirectTo: "not-found" },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      useHash: true,
      relativeLinkResolution: "legacy",
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
