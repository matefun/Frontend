import { Component } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-not-found",
  templateUrl: "./not-found.component.html",
  styleUrls: ["not-found.component.scss"],
})
export class NotFoundComponent {
  translateService: any;
  urlLogin: string;

  constructor(public translate: TranslateService) {
    this.translateService = translate;

    this.translateService.get("i18n.code").subscribe((res) => {
      this.urlLogin = "/" + res + "/login";
    });
  }
}
