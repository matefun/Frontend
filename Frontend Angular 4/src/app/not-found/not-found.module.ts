import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { I18nModule } from "../shared/modules/translate/i18n.module";
import { NotFoundComponent } from "./not-found.component";
import { NotFoundRoutingModule } from "./not-found-routing.module";

@NgModule({
  imports: [I18nModule, NotFoundRoutingModule, RouterModule],
  declarations: [NotFoundComponent],
})
export class NotFoundModule {}
