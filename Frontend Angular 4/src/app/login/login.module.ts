import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import { LoginRoutingModule } from "./login-routing.module";
import { LoginComponent } from "./login.component";
import { FormsModule } from "@angular/forms";
import { AuthenticationService } from "../shared/services/authentication.service";
import { I18nModule } from "../shared/modules/translate/i18n.module";
import { TitleCaseModule } from "../shared/modules/titlecase.module";

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    LoginRoutingModule,
    I18nModule,
    TitleCaseModule,
    // NgbModule.forRoot(),
    NgbModule,
  ],
  declarations: [LoginComponent],
  providers: [AuthenticationService],
})
export class LoginModule {}
