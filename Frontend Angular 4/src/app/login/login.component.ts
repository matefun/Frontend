import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { SessionService } from "../shared/services/session.service";
import { AuthenticationService } from "../shared/services/authentication.service";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
})
export class LoginComponent implements OnInit {
  languages = [
    { id: 0, name: "Español", code: "es", flagCode: "es" },
    { id: 1, name: "English", code: "en", flagCode: "us" },
  ];

  model: any = {
    language: this.languages[0],
  };

  loading = false;
  error = false;
  errorText = "";
  returnUrl: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private sessionService: SessionService,
    private authenticationService: AuthenticationService,
    public translate: TranslateService
  ) {}

  ngOnInit() {
    console.log("this.route", this.route.snapshot.data["language"]);
    console.log("return", this.route.snapshot.queryParams["returnUrl"]);

    // let currentSession = sessionStorage.getItem("currentUser");
    // let langCode = currentSession ? JSON.parse(currentSession).language : 'es';
    // if (langCode) {
    //     this.model.language = this.getLanguageElementByCode(langCode);
    // }

    this.model.language = this.getLanguageElementByCode(
      this.route.snapshot.data["language"]
    );
    this.translate.use(this.model.language.code);

    // reset login status
    this.authenticationService.logout();

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams["returnUrl"] || "/matefun";
  }

  login() {
    // this.router.navigate([this.returnUrl]);
    this.loading = true;

    var that = this;

    this.translate.use(this.model.language.code);
    this.authenticationService
      .login(this.model.cedula, this.model.password, this.model.language.code)
      .subscribe(
        (data) => {
          //resetSession = true;
          this.router.navigate([this.returnUrl]);
          that.sessionService.reset();
        },
        (error) => {
          this.loading = false;
          this.error = true;
          this.errorText = error.text();
        }
      );
  }

  invitado() {
    this.loading = true;

    this.authenticationService
      .login("invitado", "invitado", this.model.language.code)
      .subscribe(
        (data) => {
          this.router.navigate([this.returnUrl]);
          this.sessionService.reset();
        },
        (error) => {
          this.loading = false;
        }
      );
  }

  onChangeLanguage(lang) {
    this.model.language = lang;
    this.translate.use(this.model.language.code);
  }

  getLanguageElementByCode(code) {
    let langElement = null;
    for (let lang of this.languages) {
      if (lang.code === code) {
        langElement = lang;
        break;
      }
    }
    return langElement;
  }
}
