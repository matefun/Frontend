import { Component } from "@angular/core";
import { Router } from "@angular/router";

import { Archivo, Evaluacion } from "../../shared/objects/archivo";
import { Grupo } from "../../shared/objects/grupo";
import { Usuario } from "../../shared/objects/usuario";
import { AuthenticationService } from "../../shared/services/authentication.service";
import { HaskellService } from "../../shared/services/haskell.service";
import { SessionService } from "../../shared/services/session.service";
import { NgbPopoverConfig, NgbPopover } from "@ng-bootstrap/ng-bootstrap";
import { NotificacionService } from "../../shared/services/notificacion.service";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "grupos",
  templateUrl: "./grupos.component.html",
})
export class GruposComponent {
  archivos: Archivo[] = [];
  grupos: Grupo[] = [];
  grupoSeleccionado: Grupo = undefined;

  alumnoSeleccionado: Usuario = undefined;

  archivoSeleccionado: Archivo = undefined;

  tipoArchivo: string = undefined;

  loading: boolean = false;
  idRecorridos: any = [];
  tree: any;
  directorioActual: any;
  configCodeMirror = JSON.parse(sessionStorage.getItem("codeMirrorConfig"));
  translateService: any;

  // - - - - - - - - - - - -  Modal show confirm  - - - - - - - - - - - -
  /**
   * Con `true` se renderiza el modal de calificar entrega.
   */
  modalQualifyDelivery = false;

  /**
   * Con `true` se indica que el modal -de calificar entrega- se quiere abrir.
   * Útil para avisar al modal que anime el dismiss antes de que se elimine del
   * DOM.
   */
  modalQualifyDeliveryOpened = true;

  /**
   * Determina los estados posibles para calificar un archivo
   */
  posibleStatusesQualifyDelivery = [
    {
      value: "Corregido",
      label: "",
    },
    {
      value: "Devuelto",
      label: "",
    },
  ];

  constructor(
    private router: Router,
    private authService: AuthenticationService,
    private haskellService: HaskellService,
    private notifService: NotificacionService,
    private sessionService: SessionService,
    public translate: TranslateService
  ) {
    this.translateService = translate;
    this.directorioActual = {};
    this.directorioActual.archivos = [];
    this.configCodeMirror.readOnly = true;

    // Se obtienen las traducciones de los estados posibles de calificación
    this.translateService.get("i18n.msg.file.evaluated").subscribe((res) => {
      this.posibleStatusesQualifyDelivery[0].label = res;
    });

    this.translateService.get("i18n.msg.file.returned").subscribe((res) => {
      this.posibleStatusesQualifyDelivery[1].label = res;
    });
  }

  private notificationTypeDictonarty = {
    error: (message: string) => this.notifService.error(message),
    success: (message: string) => this.notifService.success(message),
    warning: (message: string) => this.notifService.warning(message),
  };

  ngOnInit() {
    let cedula = this.authService.getUser().cedula;
    this.loading = true;
    this.haskellService.getGrupos(cedula).subscribe(
      (grupos) => {
        this.grupos = grupos;
        this.ordenarGrupos();
        this.loading = false;
      },
      (error) => console.log(error)
    );
  }

  showNotification(type: "error" | "success" | "warning", traslation: string) {
    this.translateService
      .get(traslation)
      .subscribe(this.notificationTypeDictonarty[type]);
  }

  //ordenar archivos del grupo seleccionado.
  ordenarAlph(a, b) {
    if (a.nombre.toLowerCase() < b.nombre.toLowerCase()) return -1;
    if (a.nombre.toLowerCase() > b.nombre.toLowerCase()) return 1;
    return 0;
  }

  ordenarArchivos() {
    this.grupoSeleccionado.archivos = this.grupoSeleccionado.archivos.sort(
      this.ordenarAlph
    );
  }

  //ordeno los archivos del alumno (los archivos entregados.)
  ordenarArchivosAlumno() {
    if (this.archivoSeleccionado && this.archivoSeleccionado.archivos) {
      this.archivoSeleccionado.archivos =
        this.archivoSeleccionado.archivos.sort(this.ordenarAlph);
    }
  }

  //ordenar grupos
  ordenarGrupoF(a, b) {
    if (a.grado > b.grado) return 1;
    if (a.grado < b.grado) return -1;
    if (a.grupo.toLowerCase() > b.grupo.toLowerCase()) return 1;
    if (a.grupo.toLowerCase() < b.grupo.toLowerCase()) return -1;
    return 0;
  }

  ordenarGrupos() {
    this.grupos = this.grupos.sort(this.ordenarGrupoF);
  }

  //ordenar alumnos
  ordenarAlumnosF(a, b) {
    if (a.apellido.toLowerCase() > b.apellido.toLowerCase()) return 1;
    if (a.apellido.toLowerCase() < b.apellido.toLowerCase()) return -1;
    return 0;
  }

  ordenarAlumnos() {
    this.grupoSeleccionado.alumnos = this.grupoSeleccionado.alumnos.sort(
      this.ordenarAlumnosF
    );
  }

  seleccionarGrupo(grupo) {
    this.grupoSeleccionado = grupo;
    this.ordenarAlumnos();
    this.ordenarArchivos();
    this.archivoSeleccionado = undefined;
    this.alumnoSeleccionado = undefined;
  }

  desseleccionarGrupo() {
    this.grupoSeleccionado = undefined;
    this.archivoSeleccionado = undefined;
    this.alumnoSeleccionado = undefined;
  }

  seleccionarAlumno(alumno) {
    if (!(typeof alumno === "undefined")) {
      this.alumnoSeleccionado = alumno;
      this.ordenarArchivosAlumno();
      this.archivoSeleccionado = undefined;
    }
  }

  seleccionarArchivo(archivo) {
    this.archivoSeleccionado = archivo;
    this.alumnoSeleccionado = undefined;
    this.tipoArchivo = "compartido";
  }

  seleccionarEntrega(entrega) {
    this.archivoSeleccionado = entrega;
    this.alumnoSeleccionado = undefined;
    this.tipoArchivo = "entrega";
  }

  mostrarModalCalificarEntrega() {
    // Mostrar el modal
    this.modalQualifyDelivery = true;
    this.modalQualifyDeliveryOpened = true;
  }

  confirmFileQualify(event: CustomEvent<any>) {
    const { descripcion, estado, nota } = event.detail;

    // Si la nota está fuera de rango, se devuelve un error
    if (!(nota >= 0 && nota <= 100)) {
      this.translateService
        .get("i18n.warning.file.qualifyOutRange")
        .subscribe((res) => this.notifService.error(res));
      return;
    }

    const cedula = JSON.parse(sessionStorage.currentUser).cedula + "";

    const evaluacion = new Evaluacion();
    evaluacion.cedulaDocente = cedula;
    evaluacion.descripcion = descripcion;
    evaluacion.nota = nota;

    this.haskellService
      .calificarArchivo(this.archivoSeleccionado.id, estado, evaluacion)
      .subscribe(
        (evaluacion) => {
          this.translateService
            .get("i18n.msg.file.evaluated")
            .subscribe((res) => this.notifService.success(res));
          this.archivoSeleccionado.evaluacion = evaluacion;

          // Cerrar el modal en caso de éxito
          this.modalQualifyDeliveryOpened = false;
        },
        (error) => {
          this.notifService.error(error);

          // Cerrar el modal en caso de falla
          this.modalQualifyDeliveryOpened = false;
        }
      );
  }

  esArchivoGrupo() {
    if (
      this.archivoSeleccionado &&
      this.grupoSeleccionado &&
      this.grupoSeleccionado.archivos.some(
        (arch) => arch.id == this.archivoSeleccionado.id
      )
    ) {
      return true;
    } else {
      return false;
    }
  }

  cargarArchivoCompartido() {
    if (!this.archivoSeleccionado || this.archivoSeleccionado.directorio) {
      this.showNotification("warning", "i18n.warning.file.noSelected");
      return;
    }
    this.sessionService.setArchivo(this.archivoSeleccionado);
    this.router.navigate(["/matefun"]);
  }
}
