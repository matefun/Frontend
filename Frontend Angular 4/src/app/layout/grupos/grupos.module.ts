import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { GruposComponent } from "./grupos.component";
import { CommonModule } from "@angular/common";
import { GruposRoutingModule } from "./grupos-routing.module";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { LtCodemirrorModule } from "lt-codemirror";
import { NotificacionModule } from "../../notificacion/notificacion.module";
import { I18nModule } from "../../shared/modules/translate/i18n.module";
import { TitleCaseModule } from "../../shared/modules/titlecase.module";

@NgModule({
  imports: [
    CommonModule,
    GruposRoutingModule,
    FormsModule,
    NgbModule,
    LtCodemirrorModule,
    NotificacionModule,
    I18nModule,
    TitleCaseModule,
  ],
  declarations: [GruposComponent],
  exports: [GruposComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class GruposModule {}
