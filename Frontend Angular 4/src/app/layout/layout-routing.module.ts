import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LayoutComponent } from "./layout.component";

const routes: Routes = [
  {
    path: "",
    component: LayoutComponent,
    children: [
      {
        path: "matefun",
        loadChildren: () => import('./matefun/matefun.module').then(m => m.MateFunModule),
      },
      {
        path: "archivos",
        loadChildren: () => import('./archivos/archivos.module').then(m => m.ArchivosModule),
      },
      { path: "grupos", loadChildren: () => import('./grupos/grupos.module').then(m => m.GruposModule) },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LayoutRoutingModule {}
