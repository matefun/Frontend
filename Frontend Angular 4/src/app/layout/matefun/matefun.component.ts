import {
  Component,
  ViewChild,
  HostListener,
  ComponentRef,
  QueryList,
  ViewChildren,
} from "@angular/core";
import { HaskellService } from "../../shared/services/haskell.service";
import { WebsocketService } from "../../shared/services/websocket.service";
import { UsuarioService } from "../../shared/services/usuario.service";
import { SessionService } from "../../shared/services/session.service";
import { GHCIService } from "../../shared/services/ghci.service";
import { AuthenticationService } from "../../shared/services/authentication.service";
import { Archivo } from "../../shared/objects/archivo";
import { Configuracion } from "../../shared/objects/usuario";
import { LtCodemirrorComponent } from "lt-codemirror";
import { NgbPopoverConfig, NgbPopover } from "@ng-bootstrap/ng-bootstrap";
import { NgbPopoverWindow } from "@ng-bootstrap/ng-bootstrap/popover/popover";
import { NotificacionService } from "../../shared/services/notificacion.service";
import { Graph2DComponent } from "../plotter/graph2D/graph2D.component";
import { Graph3DComponent } from "../plotter/graph3D/graph3D.component";
import { TranslateService } from "@ngx-translate/core";
import { TitleCasePipe } from "../../shared/pipes/titlecase.pipe";
import { Router } from "@angular/router";

// import 'codemirror/mode/haskell/haskell';
import "codemirror/addon/display/panel";
import "codemirror/addon/hint/show-hint";
import "codemirror/addon/hint/anyword-hint";
import "codemirror/mode/markdown/markdown";
import "codemirror/lib/codemirror";
import "codemirror/addon/search/search";

import "codemirror/addon/dialog/dialog";
import "codemirror/addon/search/search";
import "codemirror/addon/search/matchesonscrollbar";
import "codemirror/addon/search/jump-to-line";
import "codemirror/addon/edit/matchbrackets";
import "codemirror/addon/selection/active-line";
import "codemirror/addon/comment/comment.js";

import "./codemirror/matefun-mode-ES.js";
import "./codemirror/matefun-mode-EN.js";
import "./codemirror/addons/functions_definition_EN.js";
import "./codemirror/addons/functions_definition_ES.js";

var codeMirrorRef: any;
var componentRef: any;
var focus: any;

@Component({
  selector: "matefun",
  templateUrl: "./matefun.component.html",
  styleUrls: ["./matefun.component.scss"],
  providers: [WebsocketService, NgbPopoverConfig, UsuarioService],
})
export class MateFunComponent {
  titlecasePipe: any;
  translateService: any;
  consoleDisable: boolean = false;
  consolaVisible: boolean = true;
  cursorPanel: any;
  cursorPanelLabel: any;
  cursorLabelInit: boolean = false;
  entrada: string = "";
  archivo: Archivo;
  copiaNombreArchivo: string;
  copiaContenidoArchivo: string;
  modificado = false;
  argumentoI = false;
  argumentoF = false;
  hintsCheck = true;
  typingCheck = true;
  editableLoaded = false;
  editDialogFired = false;
  archivosTree: any;
  idRecorridos: any;
  code: string = "";
  mostrandoDefinicion = false;
  archivoDefinicion: any = {};
  jump = false;
  jump_word = "";
  configCodeMirror = {
    readOnly: false,
    lineNumbers: true,
    lineWrapping: true,
    matchBrackets: true,
    styleActiveLine: true,
    extraKeys: { "Ctrl-Space": "autocomplete" },
    mode: {
      name: "matefun-EN",
      globalVars: true,
    },
    gutters: ["CodeMirror-linenumbers", "breakpoints"],
    theme: "dracula",
    fontSize: 12,
    hintOptions: {
      completeSingle: false,
      closeCharacters: /[^\d\w\_]/,
    },
    files: null,
  };
  configCodeMirrorDefinicion: any = {};
  themes = [
    "3024-day",
    "3024-night",
    "abcdef",
    "ambiance-mobile",
    "ambiance",
    "base16-dark",
    "base16-light",
    "bespin",
    "blackboard",
    "cobalt",
    "colorforth",
    "dracula",
    "duotone-dark",
    "duotone-light",
    "eclipse",
    "elegant",
    "erlang-dark",
    "hopscotch",
    "icecoder",
    "isotope",
    "lesser-dark",
    "liquibyte",
    "material",
    "mbo",
    "mdn-like",
    "midnight",
    "monokai",
    "neat",
    "neo",
    "night",
    "panda-syntax",
    "paraiso-dark",
    "paraiso-light",
    "pastel-on-dark",
    "railscasts",
    "rubyblue",
    "seti",
    "solarized",
    "the-matrix",
    "tomorrow-night-bright",
    "tomorrow-night-eighties",
    "ttcn",
    "twilight",
    "vibrant-ink",
    "xq-dark",
    "xq-light",
    "yeti",
    "zenburn",
  ];
  version: string = "3.1.3"; //npm.version;
  activeTabId = 1;

  // - - - - - - - - - - - - - Modal create file - - - - - - - - - - - - -
  /**
   * Con `true` se renderiza el modal de crear un archivo en un directorio.
   */
  modalCreateFile = false;

  /**
   * Con `true` se indica que el modal -de crear un archivo- se quiere abrir.
   * Útil para avisar al modal que anime el dismiss antes de que se elimine del
   * DOM.
   */
  modalCreateFileOpened = true;

  /**
   * Con `true` se renderiza un input para importar el contenido del archivo a
   * crear en el modal de crear archivo.
   */
  importCreateFile = false;

  // - - - - - - - - - - - -  Modal show confirm  - - - - - - - - - - - -
  /**
   * Con `true` se renderiza el modal de preguntar para editar un archivo de
   * solo lectura.
   */
  modalShowConfirm = false;

  /**
   * Con `true` se indica que el modal -de preguntar para editar un archivo de
   * solo lectura- se quiere abrir. Útil para avisar al
   * modal que anime el dismiss antes de que se elimine del DOM.
   */
  modalShowConfirmOpened = true;

  constructor(
    private haskellService: HaskellService,
    private authService: AuthenticationService,
    private ghciService: GHCIService,
    private notifService: NotificacionService,
    private sessionService: SessionService,
    private usuarioService: UsuarioService,
    public translate: TranslateService,
    private router: Router
  ) {
    // i18n
    this.translateService = translate;
    this.titlecasePipe = new TitleCasePipe();

    //si el archivo fue seteado en la session.
    this.archivo = sessionService.getArchivo();
    if (!this.archivo || !this.archivo.id) {
      this.newFile();
    }
    this.copiaContenidoArchivo = this.archivo.contenido;
    this.copiaNombreArchivo = this.archivo.nombre;
    if (authService.getUser().configuracion) {
      var config: Configuracion = authService.getUser().configuracion;
      if (config.fontSizeEditor <= 30 && config.fontSizeEditor >= 8) {
        this.configCodeMirror.fontSize = config.fontSizeEditor;
      }
      if (this.themes.some((theme) => theme == config.themeEditor)) {
        this.configCodeMirror.theme = config.themeEditor;
      }
      sessionStorage.setItem(
        "codeMirrorConfig",
        JSON.stringify(this.configCodeMirror)
      );
      this.argumentoI = config.argumentoI;
      this.argumentoF = config.argumentoF;
    }
    this.code = "my code";

    for (var key in this.configCodeMirror) {
      this.configCodeMirrorDefinicion[key] = this.configCodeMirror[key];
    }
    this.configCodeMirrorDefinicion["readOnly"] = true;
  }

  @ViewChildren(LtCodemirrorComponent)
  codemirror: QueryList<LtCodemirrorComponent>;
  // @ViewChild(NgbPopover) popover: NgbPopover;
  @ViewChild("popover") popover: NgbPopover;

  updateActiveTabId(id: number) {
    this.activeTabId = id;
  }

  updateConfig(theme) {
    this.configCodeMirror.theme = theme;
    this.codemirror.first.instance.setOption("theme", theme);
    sessionStorage.setItem(
      "codeMirrorConfig",
      JSON.stringify(this.configCodeMirror)
    );
  }

  lockSaveButton() {
    this.copiaNombreArchivo = this.archivo.nombre;
    this.copiaContenidoArchivo = this.archivo.contenido;
    this.modificado = false;
  }

  showConfirm() {
    // Mostrar el modal
    this.modalShowConfirm = true;
    this.modalShowConfirmOpened = true;
  }

  allowFileEdition() {
    codeMirrorRef.options.readOnly = false;
    componentRef.editDialogFired = true;

    // Cerrar el modal
    this.modalShowConfirmOpened = false;
  }

  /* Panel para la posición del cursor */
  makePanel() {
    var node = document.createElement("div");
    node.id = "cursorpos-panel";
    node.className = "panel bottom";
    this.cursorPanelLabel = node.appendChild(document.createElement("span"));
    var cm = this.codemirror.first.instance;
    var x = cm.getCursor().line;
    var y = cm.getCursor().ch;
    x = (Number(x) + 1).toString();
    y = (Number(y) + 1).toString();

    this.cursorPanel = this.codemirror.first.instance.addPanel(node, {
      position: "bottom",
      stable: true,
    });

    const that = this;
    this.translateService
      .get("i18n.msg.codemirror.cursorPosition")
      .subscribe((res) => {
        // Se configura la posición inicial del cursor
        this.cursorPanelLabel.textContent = res + ": (" + x + "," + y + ")";

        // Agregamos el evento que setea la posición
        this.codemirror.first.instance.on("cursorActivity", (codeMirror) => {
          let x = codeMirror.getCursor().line;
          let y = codeMirror.getCursor().ch;

          x = (Number(x) + 1).toString();
          y = (Number(y) + 1).toString();
          that.cursorPanel.node.innerText = res + ": (" + x + "," + y + ")";
        });
      });

    this.codemirror.first.instance.on("keyHandled", function (cm, name, evt) {
      if (name.code === "Digit1" && name.ctrlKey && name.shiftKey) {
        that.seleccionarDirectorio();
      } else if (name.code === "Digit2" && name.ctrlKey && name.shiftKey) {
        that.saveConfig();
      }
    });

    this.codemirror.first.instance.on("keypress", function (cm, name, evt) {
      if (
        !that.editDialogFired &&
        JSON.parse(sessionStorage.currentUser).tipo === "docente" &&
        cm.options.readOnly
      ) {
        codeMirrorRef = that.codemirror.first.instance;
        componentRef = that;
        that.showConfirm();
      }
    });
  }

  saveConfig() {
    var config = new Configuracion();
    config.themeEditor = this.configCodeMirror.theme;
    config.fontSizeEditor = this.configCodeMirror.fontSize;
    var confUser = this.authService.getUserConfig();
    var reiniciar =
      confUser.argumentoF != this.argumentoF ||
      confUser.argumentoI != this.argumentoI;
    config.argumentoF = this.argumentoF;
    config.argumentoI = this.argumentoI;
    this.usuarioService
      .actualizarConfiguracion(this.authService.getUser().cedula, config)
      .subscribe(
        (success) => {
          //this.ghciService.consoleRef.Write("Configuración guardada"  + "\n");
          this.popover.close();
          this.authService.setUserConfig(success);
          if (reiniciar) {
            this.reiniciarInterprete();
          }
        },
        (error) => {
          this.notifService.error(error);
          this.popover.close();
        }
      );
    if (!this.hintsCheck) {
      delete this.configCodeMirror.extraKeys["Ctrl-Space"];
    } else {
      this.configCodeMirror.extraKeys["Ctrl-Space"] = "autocomplete";
    }
  }

  aumentarFuente() {
    if (this.configCodeMirror.fontSize < 30) {
      this.configCodeMirror.fontSize++;
    }
  }

  disminuirFuente() {
    if (this.configCodeMirror.fontSize > 8) {
      this.configCodeMirror.fontSize--;
    }
  }

  @HostListener("document:click", ["$event"])
  private documentClicked(event: MouseEvent): void {
    // Popover is open
    if (this.popover && this.popover.isOpen()) {
      // Not clicked on self element
      if (
        !(this.popover as any)._elementRef.nativeElement.contains(event.target)
      ) {
        // Hacking typescript to access private member
        const popoverWindowRef: ComponentRef<NgbPopoverWindow> = (
          this.popover as any
        )._windowRef;

        // If clicked outside popover window
        if (!popoverWindowRef.location.nativeElement.contains(event.target)) {
          this.popover.close();
        }
      }
    }
  }

  private notificationTypeDictonarty = {
    error: (message: string) => this.notifService.error(message),
    success: (message: string) => this.notifService.success(message),
    warning: (message: string) => this.notifService.warning(message),
  };

  ngOnInit() {
    window["matefunComponent"] = { component: this };

    this.ghciService.rendered();

    this.haskellService
      .getArchivos(this.authService.getUser().cedula)
      .subscribe(
        (archivos) => {
          //.filter(function(a){return !a.eliminado})
          this.buildTreeFromList(archivos);
        },
        (error) => console.log("Error al obtener los archivos del alumno")
      );

    function KeyPress(e) {
      var evtobj = window.event ? event : e;
      if (evtobj.keyCode == 90 && evtobj.ctrlKey) {
        //alert("Ctrl+z")
      }
      if (evtobj != null && evtobj.key != null) {
        if (evtobj.key.toLowerCase() === "a" && evtobj.ctrlKey) {
          componentRef.seleccionarDirectorio();
          return false;
        } else if (evtobj.key.toLowerCase() === "e" && evtobj.ctrlKey) {
          componentRef.downloadFile();
          return false;
        } else if (evtobj.key.toLowerCase() === "r" && evtobj.ctrlKey) {
          componentRef.reiniciarInterprete();
          return false;
        } else if (evtobj.key.toLowerCase() === "g" && evtobj.ctrlKey) {
          componentRef.guardarArchivo();
          return false;
        } else if (evtobj.key.toLowerCase() === "o" && evtobj.ctrlKey) {
          document.getElementById("popover").click();
          return false;
        } else if (
          evtobj.ctrlKey &&
          evtobj.altKey &&
          evtobj.key.toLowerCase() === "p"
        ) {
          document.getElementById("ProgramBtn").click();
          var that = componentRef;
          setTimeout(function () {
            that.codemirror.first.instance.focus();
          }, 250);
          componentRef.codemirror.first.instance.focus();
          focus = "program";
          return false;
        } else if (
          evtobj.ctrlKey &&
          evtobj.altKey &&
          evtobj.key.toLowerCase() === "c"
        ) {
          componentRef.ghciService.focusConsole();
          focus = "consola";
          return false;
        } else if (
          evtobj.ctrlKey &&
          evtobj.altKey &&
          evtobj.key.toLowerCase() === "f"
        ) {
          document.getElementById("FigurasBtn").click();
          componentRef.ghciService.focusConsole();
          focus = "graficas";
          return false;
        } else if (
          evtobj.key.toLowerCase() === "p" &&
          evtobj.ctrlKey &&
          !evtobj.altKey
        ) {
          componentRef.runCode();
          return false;
        }
      }
    }
    document.onkeydown = KeyPress;

    let currentSession = sessionStorage.getItem("currentUser");
    let langCode = currentSession ? JSON.parse(currentSession).language : "es";
    if (langCode == "es") {
      this.configCodeMirror.mode.name = "matefun-ES";
    } else if (langCode == "en") {
      this.configCodeMirror.mode.name = "matefun-EN";
    }
  }

  ngAfterViewInit() {
    this.codemirror.last.instance.on("change", () => {
      this.makeAJump();
    });

    componentRef = this;
    if (this.codemirror.first.instance != null && !this.cursorLabelInit) {
      this.cursorLabelInit = true;
      this.codemirror.first.instance.setOption(
        "theme",
        this.configCodeMirror.theme
      );
      this.makePanel();
    }
    if (
      !this.editableLoaded &&
      this.codemirror.first.instance != null &&
      this.sessionService.archivo.editable !== undefined
    ) {
      try {
        var editable =
          this.sessionService.archivo.editable &&
          (this.sessionService.archivo.estado == "Edicion" ||
            this.sessionService.archivo.estado == "Devuelto");
        this.codemirror.first.instance.options.readOnly = !editable;
        this.editableLoaded = true;
      } catch (err) {
        return;
      }
    }
  }

  showNotification(type: "error" | "success" | "warning", traslation: string) {
    this.translateService
      .get(traslation)
      .subscribe(this.notificationTypeDictonarty[type]);
  }

  makeAJump() {
    if (this.jump) {
      this.jump = false;
      this.jumpExternalFile(this.jump_word);
    }
  }

  htmlEncode(value: string) {
    return value
      .replace("Prelude> ", "")
      .replace(/&/g, "&amp;")
      .replace(/\s/g, "&nbsp;")
      .replace(/"/g, "&quot;")
      .replace(/'/g, "&#39;")
      .replace(/</g, "&lt;")
      .replace(/>/g, "&gt;");
  }

  // @ViewChild(CanvasComponent) canvasC: CanvasComponent;
  @ViewChild(Graph3DComponent) graph3DComp: Graph3DComponent;

  @ViewChild(Graph2DComponent) graph2DComp: Graph2DComponent;

  funcionSTR: string = "Math.sin(x)*x*x-20";
  consola: string = "";
  command: string = "";
  tipo: number = 1;

  private onKey = function (value: string) {
    this.funcionSTR = value;
    this.archivo.contenido = value;
  };

  private writeCommand = function (value: string) {
    this.command = value.split("\n")[value.split("\n").length - 1];
  };

  private selectFunction = function () {
    this.tipo = 1;
    this.funcionSTR = "Math.sin(x)*x*x-20";
  };

  private selectElipse = function () {
    this.tipo = 2;
    this.funcionSTR = "elipse(x,y,radioX, radioY, rotacion_en_grados)";
  };

  private selectCircle = function () {
    this.tipo = 3;
    this.funcionSTR = "circulo(x,y,radio)";
  };

  private elipse = function (
    x: number,
    y: number,
    radiusX: number,
    radiusY: number,
    rotation: number
  ) {
    return [x, y, radiusX, radiusY, rotation];
  };

  private circulo = function (x: number, y: number, radius: number) {
    return [x, y, radius];
  };

  inputConsola(text: any) {
    this.entrada = text;
  }

  newFile() {
    this.archivo = new Archivo();
    this.archivo.cedulaCreador = this.authService.getUser().cedula;
    this.archivo.contenido = "";
    this.archivo.nombre = "";
    this.copiaNombreArchivo = "";
    this.copiaContenidoArchivo = "";
  }

  archivoModificado(event) {
    if (this.hintsCheck && !event.ctrlKey && !event.altKey) {
      if (/^[\w\_\d]$/.test(event.key) || event.key == "Enter") {
        this.codemirror.first.instance.options.files = this.archivosTree;
        this.codemirror.first.instance.showHint(event);
      }
      if (
        this.copiaNombreArchivo != this.archivo.nombre ||
        this.copiaContenidoArchivo != this.archivo.contenido
      ) {
        this.modificado = true;
      } else {
        this.modificado = false;
      }
    } else if (event.ctrlKey && event.shiftKey && event.key == "K")
      this.codemirror.first.instance.toggleComment();
    else if (event.altKey && event.key == ".")
      this.codemirror.first.instance.jumpToDefinition(
        this.archivosTree,
        this.codemirror.first.instance.doc,
        event
      );
  }

  goToFilesPreview(file_found, word) {
    this.mostrandoDefinicion = true;
    for (var key in this.configCodeMirror) {
      this.configCodeMirrorDefinicion[key] = this.configCodeMirror[key];
    }
    this.configCodeMirrorDefinicion["readOnly"] = true;
    this.archivoDefinicion = this.archivosTree.archivos.filter(
      (a) => a.id == parseInt(file_found.id)
    )[0];
    this.jump = true;
    this.jump_word = word;
    this.jumpExternalFile(word);
  }

  jumpExternalFile(word) {
    this.codemirror.last.instance.jumpToDefinitionByWord(
      this.codemirror.last.instance.doc,
      word
    );
  }

  clickEnEditor(event) {
    if (this.typingCheck) {
      let currentSession = sessionStorage.getItem("currentUser");
      let langCode = currentSession
        ? JSON.parse(currentSession).language
        : "es";
      if (langCode == "en")
        this.codemirror.first.instance.functionDefinitionEN(
          this.archivo.nombre,
          this.archivosTree.archivos,
          this.codemirror.first.instance.doc,
          event
        );
      else if (langCode == "es")
        this.codemirror.first.instance.functionDefinitionES(
          this.archivo.nombre,
          this.archivosTree.archivos,
          this.codemirror.first.instance.doc,
          event
        );
    }
  }

  guardarArchivo() {
    var regex = /^[A-Z]/;
    if (this.archivo.nombre.trim() == "") {
      this.notifService.error("Nombre de archivo sin especificar");
    } else if (!regex.test(this.archivo.nombre)) {
      this.notifService.error("Nombre de archivo debe iniciar con mayusula.");
    } else {
      if (this.archivo.id) {
        this.haskellService
          .editarArchivo(this.archivo.id, this.archivo)
          .subscribe(
            (archivo) => {
              //this.ghciService.consoleRef.Write("Editar archivo: " + this.archivo.nombre + "\n");
              this.archivo = archivo;
              this.lockSaveButton();
            },
            (error) => {
              this.notifService.error(error);
            }
          );
      } else {
        this.haskellService.crearArchivo(this.archivo).subscribe(
          (archivo) => {
            //this.ghciService.consoleRef.Write("Archivo creado: " + this.archivo.nombre + "\n");
            this.archivo = archivo;
            this.lockSaveButton();
          },
          (error) => {
            this.notifService.error(error);
          }
        );
      }
    }
  }
  runCode() {
    this.ghciService.setCodemirrorRef(this.codemirror.first.instance);
    this.ghciService.resetGutters();
    var regex = /^[A-Z]/;
    if (this.archivo.nombre.trim() == "") {
      this.notifService.error("Nombre de archivo sin especificar");
    } else if (!regex.test(this.archivo.nombre)) {
      this.notifService.error("Nombre de archivo debe iniciar con mayusula.");
    } else {
      var resultado = this.sessionService.cargarDependencias(this.archivo);
      if (resultado["status"] === "miss") {
        this.ghciService.outputConsole(
          "Error: No se encuentra el archivo " + resultado["nombre"] + "\n"
        );
        return;
      }
      if (this.archivo.id) {
        if (
          this.archivo.editable ||
          this.authService.getUser().tipo == "docente"
        ) {
          this.haskellService
            .editarArchivo(this.archivo.id, this.archivo)
            .subscribe(
              (archivo) => {
                this.archivo = archivo;
                var list = this.sessionService.getDependencias(),
                  idList = [];
                for (var l in list) {
                  idList.push(list[l].id);
                }
                if (!idList.some((id) => id == archivo.id)) {
                  idList.push(archivo.id);
                }
                this.lockSaveButton();
                this.ghciService.loadFile(archivo.id, archivo.nombre, idList);
              },
              (error) => {
                this.notifService.error(error);
              }
            );
        } else {
          var list = this.sessionService.getDependencias(),
            idList = [];
          for (var l in list) {
            idList.push(list[l].id);
          }
          if (!idList.some((id) => id == this.archivo.id)) {
            idList.push(this.archivo.id);
          }
          this.ghciService.loadFile(
            this.archivo.id,
            this.archivo.nombre,
            idList
          );
        }
      } else {
        this.haskellService.crearArchivo(this.archivo).subscribe(
          (archivo) => {
            this.archivo = archivo;
            this.lockSaveButton();
            this.ghciService.loadFile(archivo.id, archivo.nombre, []);
          },
          (error) => {
            this.notifService.error(error);
          }
        );
      }
    }
    this.ghciService.focusConsole();
  }

  download(filename, text) {
    var element = document.createElement("a");
    element.setAttribute(
      "href",
      "data:application/octet-stream," + encodeURIComponent(text)
    );
    element.setAttribute("download", filename + ".mf");

    element.style.display = "none";
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
  }
  downloadFile() {
    var nom = this.archivo.nombre;
    var content = this.archivo.contenido;
    if (
      nom != undefined &&
      nom != "" &&
      content != undefined &&
      content != ""
    ) {
      this.download(nom, content);
    }
  }
  reiniciarInterprete() {
    this.ghciService.reiniciarInterprete();
  }

  toggleConsole() {
    this.consolaVisible = !this.consolaVisible;
  }

  seleccionarDirectorio(importFileContent = false) {
    this.archivosTree = this.sessionService.getArchivos(undefined);

    this.importCreateFile = importFileContent;

    // Mostrar el modal
    this.modalCreateFile = true;
    this.modalCreateFileOpened = true;
  }

  confirmFileCreation(event: CustomEvent) {
    const fileContent = event.detail.fileContent;
    const nombre = event.detail.nombre;
    const padreId = event.detail.padreId;

    if (nombre == undefined || nombre == "") {
      this.showNotification("error", "i18n.warning.file.invalidName");
      return;
    }

    /** Expresión regular para chequear que el nombre esté empiece con mayúscula */
    var regex = /^[A-Z]/;

    if (!regex.test(nombre)) {
      this.showNotification("error", "i18n.warning.file.capitalLetter");
      return;
    }

    var archivo: Archivo = new Archivo();
    archivo.cedulaCreador = this.authService.getUser().cedula;
    archivo.contenido = fileContent || "";
    archivo.nombre = nombre;
    archivo.directorio = false;
    archivo.padreId = padreId;
    archivo.editable = true;

    this.haskellService.crearArchivo(archivo).subscribe(
      (archivo) => {
        this.archivo = archivo;
        //this.parent.ghciService.loadFile(archivo.id);
        this.sessionService.setArchivo(archivo);

        // Cerrar el modal en caso de exito
        this.modalCreateFileOpened = false;
      },
      (error) => {
        this.notifService.error(error.text());

        // Cerrar el modal en caso de falla
        this.modalCreateFileOpened = false;
      }
    );
  }

  navigateBack(event: CustomEvent<Archivo>) {
    const { padreId } = event.detail;

    // No se puede navegar al padre de root
    if (padreId == -1) {
      return;
    }

    const archivosList: Archivo[] = this.sessionService.getArchivosList();

    // Se queda con el archivo identificado por padreId
    this.archivosTree = archivosList.filter((file) => file.id === padreId)[0];
  }

  buildTreeFromList(archivos) {
    this.sessionService.setArchivosList(archivos);
    var root: Archivo;

    for (var a in archivos) {
      var arch = archivos[a];
      if (arch.padreId === -1) {
        root = arch;
      }
    }
    this.idRecorridos = [root.id];
    var archivos2 = archivos.filter(function (a) {
      return a.id !== root.id;
    });
    var tree = this.buildTree(archivos2, root);
    this.archivosTree = tree;
    this.sessionService.setArchivosTree(tree);
  }

  buildTree(archivos, root) {
    root.archivos = this.getArchivos(root.id, archivos);
    for (var a in root.archivos) {
      if (
        root.archivos[a].directorio &&
        this.idRecorridos[root.archivos[a].id] === undefined
      ) {
        var id = root.archivos[a].id;
        var archivos2 = archivos.filter(function (a) {
          return a.id !== id;
        });
        root.archivos[a] = this.buildTree(archivos2, root.archivos[a]);
      }
    }
    return root;
  }

  getArchivos(id, archivos) {
    return archivos.filter(function (a) {
      return a.padreId === id;
    });
  }

  onChangeTab(selectedTabId: number, shouldUpdateActiveTabId: boolean) {
    if (shouldUpdateActiveTabId) {
      this.activeTabId = selectedTabId;
    }

    if (selectedTabId == 2) {
      setTimeout(() => {
        this.graph2DComp.onResize(null);
      });
    } else if (selectedTabId == 3) {
      this.graph3DComp.onActivate();
      this.graph3DComp.onResize(null);
    }
  }
}
