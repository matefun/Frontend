var mac = /Mac/.test(navigator.platform);

(function(mod) {
  if (typeof exports == "object" && typeof module == "object") // CommonJS
    mod(require("codemirror/lib/codemirror"));
  else if (typeof define == "function" && define.amd) // AMD
    define(["codemirror/lib/codemirror"], mod);
  else // Plain browser env
    mod(CodeMirror);
})(function(CodeMirror) {
  "use strict";
  
  const hintWords = require('./hint_words');

  var HINT_ELEMENT_CLASS = "CodeMirror-function-definition";
  
  function parseOptions(cm, pos, options) {
    var editor = cm.options.hintOptions;
    var out = {};
    for (var prop in defaultOptions) out[prop] = defaultOptions[prop];
    if (editor) for (var prop in editor)
      if (editor[prop] !== undefined) out[prop] = editor[prop];
    if (options) for (var prop in options)
      if (options[prop] !== undefined) out[prop] = options[prop];
    if (out.hint.resolve) out.hint = out.hint.resolve(cm, pos)
    return out;
  }
  
  function Completion(cm, options, filename, archivos) {
    this.filename = filename;
    this.archivos = archivos;
    this.cm = cm;
    this.options = options;
    this.widget = null;
    this.debounce = 0;
    this.tick = 0;
    this.startPos = this.cm.getCursor("start");
    this.startLen = this.cm.getLine(this.startPos.line).length - this.cm.getSelection().length;
  
    var self = this;
    cm.on("cursorActivity", this.activityFunc = function() { self.cursorActivity(); });
  }
  
  var requestAnimationFrame = window.requestAnimationFrame || function(fn) {
    return setTimeout(fn, 1000/60);
  };
  var cancelAnimationFrame = window.cancelAnimationFrame || clearTimeout;
  
  Completion.prototype = {
    close: function() {
      if (!this.active()) return;
      this.cm.state.completionActive = null;
      this.tick = null;
      this.cm.off("cursorActivity", this.activityFunc);
  
      if (this.widget && this.data) CodeMirror.signal(this.data, "close");
      if (this.widget) this.widget.close();
      CodeMirror.signal(this.cm, "endFunctionDefinition", this.cm);
    },
  
    active: function() {
      return this.cm.state.completionActive == this;
    },
  
    pick: function(data, i) {
      var completion = data.list[i];
      if (completion.hint) completion.hint(this.cm, data, completion);
      else this.cm.replaceRange(getText(completion), completion.from || data.from,
                                completion.to || data.to, "complete");
      CodeMirror.signal(data, "pick", completion);
      this.close();
    },
  
    cursorActivity: function() {
      if (this.debounce) {
        cancelAnimationFrame(this.debounce);
        this.debounce = 0;
      }
  
      var pos = this.cm.getCursor(), line = this.cm.getLine(pos.line);
      if (pos.line != this.startPos.line || line.length - pos.ch != this.startLen - this.startPos.ch ||
          pos.ch < this.startPos.ch || this.cm.somethingSelected() ||
          (!pos.ch || this.options.closeCharacters.test(line.charAt(pos.ch - 1)))) {
        this.close();
      } else {
        var self = this;
        this.debounce = requestAnimationFrame(function() {self.update();});
        if (this.widget) this.widget.disable();
      }
    },
  
    update: function(first) {
      if (this.tick == null) return
      var self = this, myTick = ++this.tick
      fetchHints(this.options.hint, this.cm, this.options, function(data) {
        if (self.tick == myTick) self.finishUpdate(data, first)
      })
    },
  
    finishUpdate: function(data, first) {
      if (this.data) CodeMirror.signal(this.data, "update");
  
      var picked = (this.widget && this.widget.picked) || (first && this.options.completeSingle);
      if (this.widget) this.widget.close();
  
      this.data = data;
  
      if (data && data.list.length) {
        if (picked && data.list.length == 1) {
          this.pick(data, 0);
        } else {
          this.widget = new Widget(this, data);
          CodeMirror.signal(data, "shown");
        }
      }
    }
  };
  
  function getText(completion) {
    if (typeof completion == "string") return completion;
    else return completion.text;
  }
  
  function buildKeyMap(completion, handle) {
    var baseMap = {
      Up: function() {handle.moveFocus(-1);},
      Down: function() {handle.moveFocus(1);},
      PageUp: function() {handle.moveFocus(-handle.menuSize() + 1, true);},
      PageDown: function() {handle.moveFocus(handle.menuSize() - 1, true);},
      Home: function() {handle.setFocus(0);},
      End: function() {handle.setFocus(handle.length - 1);},
      Enter: handle.pick,
      Tab: handle.pick,
      Esc: handle.close
    };
  
    if (mac) {
      baseMap["Ctrl-P"] = function() {handle.moveFocus(-1);};
      baseMap["Ctrl-N"] = function() {handle.moveFocus(1);};
    }
  
    var custom = completion.options.customKeys;
    var ourMap = custom ? {} : baseMap;
    function addBinding(key, val) {
      var bound;
      if (typeof val != "string")
        bound = function(cm) { return val(cm, handle); };
      // This mechanism is deprecated
      else if (baseMap.hasOwnProperty(val))
        bound = baseMap[val];
      else
        bound = val;
      ourMap[key] = bound;
    }
    if (custom)
      for (var key in custom) if (custom.hasOwnProperty(key))
        addBinding(key, custom[key]);
    var extra = completion.options.extraKeys;
    if (extra)
      for (var key in extra) if (extra.hasOwnProperty(key))
        addBinding(key, extra[key]);
    return ourMap;
  }
  
  function getHintElement(hintsElement, el) {
    while (el && el != hintsElement) {
      if (el.nodeName.toUpperCase() === "LI" && el.parentNode == hintsElement) return el;
      el = el.parentNode;
    }
  }
  
  function Widget(completion, data) {
    this.completion = completion;
    this.data = data;
    this.picked = false;
    var widget = this, cm = completion.cm;
    var ownerDocument = cm.getInputField().ownerDocument;
    var parentWindow = ownerDocument.defaultView || ownerDocument.parentWindow;
  
    var hints = this.hints = ownerDocument.createElement("ul");
    var theme = completion.cm.options.theme;
    hints.className = "CodeMirror-hints " + theme;
    this.selectedHint = data.selectedHint || 0;
  
    var completions = data.list;
    for (var i = 0; i < completions.length; ++i) {
      var elt = hints.appendChild(ownerDocument.createElement("li")), cur = completions[i];
      var className = HINT_ELEMENT_CLASS;
      if (cur.className != null) className = cur.className + " " + className;
      elt.className = className;
      if (cur.render) cur.render(elt, data, cur);
      else { 
        var rexp_name = new RegExp(/(.*\:\:)(.*)/.source, "g");
        var m = rexp_name.exec(cur.displayText || getText(cur));
        var b_elem = ownerDocument.createElement("b"); 
        b_elem.appendChild(ownerDocument.createTextNode(m[1]));
        elt.appendChild(b_elem); 
        elt.appendChild(ownerDocument.createTextNode(m[2])); 
      }
      elt.hintId = i;
    }
  
    var pos = cm.cursorCoords(completion.options.alignWithWord ? data.from : null);
    // If we're at the edge of the screen, then we want the menu to appear on the left of the cursor.
    var winW = parentWindow.innerWidth || Math.max(ownerDocument.body.offsetWidth, ownerDocument.documentElement.offsetWidth);
    var winH = parentWindow.innerHeight || Math.max(ownerDocument.body.offsetHeight, ownerDocument.documentElement.offsetHeight);
    (completion.options.container || ownerDocument.body).appendChild(hints);
    var box = hints.getBoundingClientRect(), overlapY = box.bottom - winH;
    var scrolls = hints.scrollHeight > hints.clientHeight + 1
    var startScroll = cm.getScrollInfo();
  
    var height = box.bottom - box.top;
    var left = pos.left - 1;
    hints.style.left = left + "px";
    hints.style.top = (pos.top - height) + "px";
    var below = false;

    if (overlapY > 0 && height > winH) {
      hints.style.height = (winH - 5) + "px";
      hints.style.top = (top = pos.bottom - box.top) + "px";
      var cursor = cm.getCursor();
      if (data.from.ch != cursor.ch) {
        pos = cm.cursorCoords(cursor);
        hints.style.left = (left = pos.left) + "px";
        box = hints.getBoundingClientRect();
      }
    }
    var overlapX = box.right - winW;
    if (overlapX > 0) {
      if (box.right - box.left > winW) {
        hints.style.width = (winW - 5) + "px";
        overlapX -= (box.right - box.left) - winW;
      }
      hints.style.left = (left = pos.left - overlapX) + "px";
    }
    if (scrolls) for (var node = hints.firstChild; node; node = node.nextSibling)
      node.style.paddingRight = cm.display.nativeBarWidth + "px"
  
    cm.addKeyMap(this.keyMap = buildKeyMap(completion, {
      moveFocus: function(n, avoidWrap) { widget.changeActive(widget.selectedHint + n, avoidWrap); },
      setFocus: function(n) { widget.changeActive(n); },
      menuSize: function() { return widget.screenAmount(); },
      length: completions.length,
      close: function() { completion.close(); },
      pick: function() { widget.pick(); },
      data: data
    }));
  
    if (completion.options.closeOnUnfocus) {
      var closingOnBlur;
      cm.on("blur", this.onBlur = function() { closingOnBlur = setTimeout(function() { completion.close(); }, 100); });
      cm.on("focus", this.onFocus = function() { clearTimeout(closingOnBlur); });
    }
  
    cm.on("scroll", this.onScroll = function() {
      var curScroll = cm.getScrollInfo(), editor = cm.getWrapperElement().getBoundingClientRect();
      var newTop = top + startScroll.top - curScroll.top;
      var point = newTop - (parentWindow.pageYOffset || (ownerDocument.documentElement || ownerDocument.body).scrollTop);
      if (!below) point += hints.offsetHeight;
      if (point <= editor.top || point >= editor.bottom) return completion.close();
      hints.style.top = newTop + "px";
      hints.style.left = (left + startScroll.left - curScroll.left) + "px";
    });
  
    CodeMirror.on(hints, "dblclick", function(e) {
      completion.close();
    });
  
    CodeMirror.on(hints, "click", function(e) {
      completion.close();
    });
  
    CodeMirror.on(hints, "mousedown", function() {
      setTimeout(function(){cm.focus();}, 20);
    });
  
    CodeMirror.signal(data, "select", completions[this.selectedHint], hints.childNodes[this.selectedHint]);
    return true;
  }
  
  Widget.prototype = {
    close: function() {
      if (this.completion.widget != this) return;
      this.completion.widget = null;
      this.hints.parentNode.removeChild(this.hints);
      this.completion.cm.removeKeyMap(this.keyMap);
  
      var cm = this.completion.cm;
      if (this.completion.options.closeOnUnfocus) {
        cm.off("blur", this.onBlur);
        cm.off("focus", this.onFocus);
      }
      cm.off("scroll", this.onScroll);
    },
  
    disable: function() {
      this.completion.cm.removeKeyMap(this.keyMap);
      var widget = this;
      this.keyMap = {Enter: function() { widget.picked = true; }};
      this.completion.cm.addKeyMap(this.keyMap);
    },
  
    pick: function() {
      this.completion.pick(this.data, this.selectedHint);
    },
  
    changeActive: function(i, avoidWrap) {
      if (i >= this.data.list.length)
        i = avoidWrap ? this.data.list.length - 1 : 0;
      else if (i < 0)
        i = avoidWrap ? 0  : this.data.list.length - 1;
      if (this.selectedHint == i) return;
      var node = this.hints.childNodes[this.selectedHint];
      if (node) node.className = node.className.replace(" " + ACTIVE_HINT_ELEMENT_CLASS, "");
      node = this.hints.childNodes[this.selectedHint = i];
      node.className += " " + ACTIVE_HINT_ELEMENT_CLASS;
      if (node.offsetTop < this.hints.scrollTop)
        this.hints.scrollTop = node.offsetTop - 3;
      else if (node.offsetTop + node.offsetHeight > this.hints.scrollTop + this.hints.clientHeight)
        this.hints.scrollTop = node.offsetTop + node.offsetHeight - this.hints.clientHeight + 3;
      CodeMirror.signal(this.data, "select", this.data.list[this.selectedHint], node);
    },
  
    screenAmount: function() {
      return Math.floor(this.hints.clientHeight / this.hints.firstChild.offsetHeight) || 1;
    }
  };
  
  function applicableHelpers(cm, helpers) {
    if (!cm.somethingSelected()) return helpers
    var result = []
    for (var i = 0; i < helpers.length; i++)
      if (helpers[i].supportsSelection) result.push(helpers[i])
    return result
  }
  
  function fetchHints(hint, cm, options, callback) {
    if (hint.async) {
      functionDefinition(cm, callback, options)
    } else {
      var result = functionDefinition(cm, options)
      if (result && result.then) result.then(callback)
      else callback(result)
    }
  }
  
  function resolveAutoHints(cm, pos) {
    var helpers = cm.getHelpers(pos, "functionDefinitionEN"), words
    if (helpers.length) {
      var resolved = function(cm, callback, options) {
        var app = applicableHelpers(cm, helpers);
        function run(i) {
          if (i == app.length) return callback(null)
          fetchHints(app[i], cm, options, function(result) {
            if (result && result.list.length > 0) callback(result)
            else run(i + 1)
          })
        }
        run(0)
      }
      resolved.async = true
      resolved.supportsSelection = true
      return resolved
    } else if (words = cm.getHelper(cm.getCursor(), "hintWords")) {
      return function(cm) { return CodeMirror.functionDefinition.fromList(cm, {words: words}) }
    } else if (CodeMirror.functionDefinition.anyword) {
      return function(cm, options) { return CodeMirror.functionDefinition.anyword(cm, options) }
    } else {
      return function() {}
    }
  }
  
  CodeMirror.registerHelper("functionDefinitionEN", "auto", {
    resolve: resolveAutoHints
  });
  
  CodeMirror.registerHelper("functionDefinitionEN", "fromList", function(cm, options) {
    var cur = cm.getCursor(), token = cm.getTokenAt(cur)
    var term, from = CodeMirror.Pos(cur.line, token.start), to = cur
    if (token.start < cur.ch && /\w/.test(token.string.charAt(cur.ch - token.start - 1))) {
      term = token.string.substr(0, cur.ch - token.start)
    } else {
      term = ""
      from = cur
    }
    var found = [];
    for (var i = 0; i < options.words.length; i++) {
      var word = options.words[i];
      if (word.slice(0, term.length) == term)
        found.push(word);
    }
  
    if (found.length) return {list: found, from: from, to: to};
  });
  
  var defaultOptions = {
    hint: CodeMirror.hint.auto,
    completeSingle: true,
    alignWithWord: true,
    closeCharacters: /./,
    closeOnUnfocus: true,
    completeOnSingleClick: true,
    container: null,
    customKeys: null,
    extraKeys: null
  };

  var WORD = /[\w$]+/;
  var RANGE = 500;

  var COMPLETE_FUNS = /([\w\_\d\-]+)(\s+\:\:.*)/;
  
  // functions definitions

  CodeMirror.defineExtension("functionDefinitionEN", function(filename, archivos, options) {
    if (options.mode.name == "matefun-EN"){
      options = parseOptions(this, this.getCursor("start"), options);
      if (this.state.completionActive) this.state.completionActive.close();
      var completion = this.state.completionActive = new Completion(this, options, filename, archivos);

      CodeMirror.signal(this, "startFunctionDefinition", this);
      completion.update(true);
    }
  });

  CodeMirror.defineExtension("closeFunctionDefinition", function() {
    if (this.state.completionActive) this.state.completionActive.close()
  })

  var INCLUIR = /include\s+([\w\_\d]+)/;

  function functionDefinition(editor, options) {
    var word = options && options.word || WORD;
    var range = options && options.range || RANGE;
    var cur = editor.getCursor(), curLine = editor.getLine(cur.line);
    var end = cur.ch, start = end;
    while (start && word.test(curLine.charAt(start - 1))) --start;
    while (end && word.test(curLine.charAt(end + 1))) ++end;
    var curWord = start != end && curLine.slice(start, end + 1);

    if (curWord.length > 0){
      var includes = new RegExp(INCLUIR.source, "g");
      var content = editor.getValue();

      var list = [];

      var file;
      while (file = includes.exec(content)) {
        var files = editor.state.completionActive.archivos.filter(
          function(a){
              return a.nombre === file[1];
          });
        for (var f = 0; f < files.length; f++){
          var seen = {};
          var re_digits = new RegExp(/^\d+$/, "g");
          var re_funs = new RegExp(COMPLETE_FUNS.source, "g");
          var m;
          
          var file_content = files[f].contenido;

          while (m = re_funs.exec(file_content)) {
            if (!re_digits.exec(m[1]) && (!curWord || m[1] == curWord) && !Object.prototype.hasOwnProperty.call(seen, m[1])) {
              seen[m[1]] = true;
              list.push(files[f].nombre + "." + m[1] + m[2]);
            }
          }
        }
      }

      // current file
      var seen = {};
      var re_digits = new RegExp(/^\d+$/, "g");

      var re_funs = new RegExp(COMPLETE_FUNS.source, "g");
      for (var dir = -1; dir <= 1; dir += 2) {
        var line = cur.line, endLine = Math.min(Math.max(line + dir * range, editor.firstLine()), editor.lastLine()) + dir;
        for (; line != endLine; line += dir) {
          var text = editor.getLine(line), m;
          while (m = re_funs.exec(text)) {
            if (!re_digits.exec(m[1]) && (!curWord || m[1] == curWord) && !Object.prototype.hasOwnProperty.call(seen, m[1])) {
              seen[m[1]] = true;
              list.push(editor.state.completionActive.filename + "." + m[1] + m[2]);
            }
          }
        }
      }

      // add default functions
      var default_functions = hintWords.EN_typed_functions();
      for (var i = 0; i < default_functions.length; i++) {
        re_funs = new RegExp(COMPLETE_FUNS.source, "g");
        var m = re_funs.exec(default_functions[i]);
        if (m[1] == curWord) {
          list.push(m[1] + m[2]);
        }
      } 

      return {list: list, from: CodeMirror.Pos(cur.line, start), to: CodeMirror.Pos(cur.line, end)};
    }
  };
});
