import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { CanvasModule } from "../canvas/canvas.module";
import { MateFunComponent } from "./matefun.component";
import { CommonModule } from "@angular/common";
import { MateFunRoutingModule } from "./matefun-routing.module";
import { LtCodemirrorModule } from "lt-codemirror";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { NotificacionModule } from "../../notificacion/notificacion.module";
import { Graph2DModule } from "../plotter/graph2D/graph2D.module";
import { Graph3DModule } from "../plotter/graph3D/graph3D.module";
import { I18nModule } from "../../shared/modules/translate/i18n.module";
import { TitleCaseModule } from "../../shared/modules/titlecase.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CanvasModule,
    Graph2DModule,
    Graph3DModule,
    NotificacionModule,
    MateFunRoutingModule,
    LtCodemirrorModule,
    NgbModule,
    I18nModule,
    TitleCaseModule,
  ],
  declarations: [MateFunComponent],
  exports: [MateFunComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class MateFunModule {}
