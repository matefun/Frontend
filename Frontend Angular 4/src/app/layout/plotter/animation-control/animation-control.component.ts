import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "animation-control",
  templateUrl: "./animation-control.component.html",
  styleUrls: ["./animation-control.component.scss"],
})
export class AnimationControlComponent implements OnInit {
  @Input() visible: boolean;
  @Input() playing: boolean;

  @Input() value: number;

  @Input() minSpeed: number;
  @Input() speed: number;

  @Input() onTogglePlay: Function;
  @Input() onChangeSpeed: Function;

  constructor() {}

  ngOnInit() {}
}
