/**
 * This barrel file provides the export for the lazy loaded BlankpageComponent.
 */
export * from "./graph2D.component";

export * from "./graph2D.routes";
