import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { FormsModule } from "@angular/forms";
import { Graph2DComponent } from "./graph2D.component";
import { DirectivesModule } from "../../../shared/directives/directives.module";
import { I18nModule } from "../../../shared/modules/translate/i18n.module";
import { TitleCaseModule } from "../../../shared/modules/titlecase.module";

@NgModule({
  imports: [
    FormsModule,
    RouterModule,
    CommonModule,
    NgbModule,
    DirectivesModule,
    I18nModule,
    TitleCaseModule,
  ],
  declarations: [Graph2DComponent],
  exports: [Graph2DComponent],
})
export class Graph2DModule {}
