import { Route } from "@angular/router";
import { Graph2DComponent } from ".";

export const Graph2DRoutes: Route[] = [
  {
    path: "graph2D",
    component: Graph2DComponent,
  },
];
