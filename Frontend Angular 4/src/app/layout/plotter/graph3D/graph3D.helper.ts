export function formatJSON(jsonString: string): string {
  const regexRot = /\"rot\"\:\((\-?\d*.\d*),(\-?\d*.\d*),(\-?\d*.\d*)\)/g;
  const regexPts =
    /\"pts\"\:\[\((\-?\d*.\d*),(\-?\d*.\d*),(\-?\d*.\d*)\),\((\-?\d*.\d*),(\-?\d*.\d*),(\-?\d*.\d*)\)\]/g;

  return jsonString
    .replace(regexRot, (match, x, y, z) => {
      return `"rot": { "x": ${x}, "y": ${y}, "z": ${z} }`;
    })
    .replace(regexPts, (match, x1, y1, z1, x2, y2, z2) => {
      return `"pts": [{ "x": ${x1}, "y": ${y1}, "z": ${z1} },{ "x": ${x2}, "y": ${y2}, "z": ${z2} }]`;
    });
}

export interface AnimationProps {
  visible?: boolean;
  playing?: boolean;
  value?: number;
  speed?: number;
}

export enum Zoom3DType {
  Normal = 1,
  XAxis,
  YAxis,
  ZAxis,
}

export interface GraphProps {
  zoomType?: Zoom3DType;
  showAxes?: boolean;
  showEdges?: boolean;
  showMetaData?: boolean;
  showVertices?: boolean;
  quality?: number;
  range: {
    xMin?: number;
    xMax?: number;
    yMin?: number;
    yMax?: number;
    zMin?: number;
    zMax?: number;
  };
}

export const Default_GraphProps: GraphProps = {
  zoomType: Zoom3DType.Normal,
  showAxes: true,
  showEdges: false,
  showMetaData: false,
  showVertices: false,
  quality: 30,
  range: {
    xMin: -10,
    xMax: 10,
    yMin: -10,
    yMax: 10,
    zMin: -10,
    zMax: 10,
  },
};

export function debounce(fn, delay) {
  var timer = null;
  return function () {
    var context = this,
      args = arguments;
    clearTimeout(timer);
    timer = setTimeout(function () {
      fn.apply(context, args);
    }, delay);
  };
}
