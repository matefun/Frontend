import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { FormsModule } from "@angular/forms";
import { Graph3DComponent } from "./graph3D.component";
import { AnimationControlComponent } from "../animation-control/animation-control.component";
import { DirectivesModule } from "../../../shared/directives/directives.module";
import { I18nModule } from "../../../shared/modules/translate/i18n.module";
import { TitleCaseModule } from "../../../shared/modules/titlecase.module";

@NgModule({
  imports: [
    FormsModule,
    RouterModule,
    CommonModule,
    NgbModule,
    DirectivesModule,
    I18nModule,
    TitleCaseModule,
  ],
  declarations: [AnimationControlComponent, Graph3DComponent],
  exports: [Graph3DComponent],
})
export class Graph3DModule {}
