import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";

import { Graph3DComponent } from "./graph3D.component";

describe("Graph3DComponent", () => {
  let component: Graph3DComponent;
  let fixture: ComponentFixture<Graph3DComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [Graph3DComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Graph3DComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should be created", () => {
    expect(component).toBeTruthy();
  });
});
