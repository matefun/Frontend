import { Component, ViewChild } from "@angular/core";
import { Archivo, Grupo } from "../../shared/objects/archivo";
import { AuthenticationService } from "../../shared/services/authentication.service";
import { HaskellService } from "../../shared/services/haskell.service";
import { SessionService } from "../../shared/services/session.service";
import { NotificacionService } from "../../shared/services/notificacion.service";
import { Router, ActivatedRoute } from "@angular/router";
import { LtCodemirrorComponent } from "lt-codemirror";
import { NgbPopoverConfig, NgbPopover } from "@ng-bootstrap/ng-bootstrap";
import { TranslateService } from "@ngx-translate/core";
import { TitleCasePipe } from "../../shared/pipes/titlecase.pipe";

import "codemirror/mode/haskell/haskell";
import "codemirror/addon/display/panel";
import "codemirror/addon/hint/show-hint";
import "codemirror/addon/hint/anyword-hint";
import "codemirror/mode/markdown/markdown";

const STARTS_WITH_CAPITAL_LETTER_REGEX = /^[A-Z]/;

const ID_ROOT_DIR = -1;

@Component({
  selector: "archivos",
  templateUrl: "./archivos.component.html",
})
export class ArchivosComponent {
  translateService: any;
  titlecasePipe: any;
  archivos: Archivo[] = [];
  archivosCompartidos: Archivo[] = [];
  archivosCompartidosSinDuplicados: Archivo[] = [];
  archivoSeleccionado: Archivo;
  loading: boolean = false;
  loadingCompartidos: boolean = false;
  filtroNombre: string = "";
  idRecorridos: any = [];
  esAlumno: boolean;
  tree: any;
  preview: string = "";
  directorioActual: any;
  sortFunction: any;
  configCodeMirror = JSON.parse(sessionStorage.getItem("codeMirrorConfig"));

  /**
   * Determina la ruta en donde se encuentra ubicado el archivo/directorio
   * seleccionado actualmente.
   */
  currentPath: string = "/";

  /**
   * Con `true` se configura el modal para que se muestre la interfaz de
   * agregar/borrar archivo. De otro modo, se muestra la interfaz de
   * agregar/borrar directorio
   */
  modalTypeIsFile = true;

  // - - - - - - - - - - - - - Modal create file - - - - - - - - - - - - -
  /**
   * Con `true` se renderiza el modal de crear un archivo
   */
  modalCreateFile = false;

  /**
   * Con `true` se indica que el modal -de crear un archivo- se quiere abrir.
   * Útil para avisar al modal que anime el dismiss antes de que se elimine del
   * DOM
   */
  modalCreateFileOpened = true;

  // - - - - - - - - - - - - -  Modal move file  - - - - - - - - - - - - -
  /**
   * Con `true` se renderiza el modal de mover un archivo
   */
  modalMoveFile = false;

  /**
   * Con `true` se indica que el modal -de mover un archivo- se quiere abrir.
   * Útil para avisar al modal que anime el dismiss antes de que se elimine del
   * DOM
   */
  modalMoveFileOpened = true;

  /**
   * Directorio actual sobre el cual será desplegada la lista de directorios
   * del modal mover archivo.
   */
  currentDirOfFileToMove: Archivo;

  // - - - - - - - - - - - - - Modal remove file - - - - - - - - - - - - -
  /**
   * Con `true` se renderiza el modal de borrar un archivo
   */
  modalRemoveFile = false;

  /**
   * Con `true` se indica que el modal -de borrar un archivo- se quiere abrir.
   * Útil para avisar al modal que anime el dismiss antes de que se elimine del
   * DOM
   */
  modalRemoveFileOpened = true;

  /**
   * Nombre del archivo que se desea borrar en el modal de borrar
   * archivos/directorios.
   */
  fileNameToRemove: { fileName: string };

  // - - - - - - - - - - - - - Modal share file - - - - - - - - - - - - -
  /**
   * Con `true` se renderiza el modal de compartir un archivo
   */
  modalShareFile = false;

  /**
   * Con `true` se indica que el modal -de compartir un archivo- se quiere abrir.
   * Útil para avisar al modal que anime el dismiss antes de que se elimine del
   * DOM
   */
  modalShareFileOpened = true;

  /**
   * Nombre del archivo que se desea compartir en el modal de compartir archivo.
   */
  fileNameToShare: { fileName: string };

  /**
   * Arreglo de grupos sobre los cuales se puede compartir el archivo
   * seleccionado en el modal de compartir archivo.
   */
  groupsToShareFile: Grupo[];

  // - - - - - - - - - - - - - Modal send file - - - - - - - - - - - - -
  /**
   * Con `true` se renderiza el modal de enviar un archivo para calificar
   */
  modalSendFile = false;

  /**
   * Con `true` se indica que el modal -de enviar un archivo para calificar-
   * se quiere abrir.
   * Útil para avisar al modal que anime el dismiss antes de que se elimine del
   * DOM
   */
  modalSendFileOpened = true;

  // - - - - - - - - - - - - - Modal see score - - - - - - - - - - - - -
  /**
   * Con `true` se renderiza el modal de ver calificacion
   */
  modalSeeScore = false;

  /**
   * Con `true` se indica que el modal -de ver calificacion- se quiere abrir.
   * Útil para avisar al modal que anime el dismiss antes de que se elimine del
   * DOM
   */
  modalSeeScoreOpened = true;

  private notificationTypeDictonarty = {
    error: (message: string) => this.notifService.error(message),
    success: (message: string) => this.notifService.success(message),
    warning: (message: string) => this.notifService.warning(message),
  };

  constructor(
    private router: Router,
    private notifService: NotificacionService,
    private authService: AuthenticationService,
    private haskellService: HaskellService,
    private sessionService: SessionService,
    public translate: TranslateService,
    private route: ActivatedRoute
  ) {
    this.translateService = translate;
    this.titlecasePipe = new TitleCasePipe();

    this.esAlumno =
      JSON.parse(sessionStorage.getItem("currentUser")).tipo === "alumno";
    this.directorioActual = {};
    this.directorioActual.archivos = [];
    this.configCodeMirror.readOnly = true;
  }
  @ViewChild(LtCodemirrorComponent)
  codemirror: LtCodemirrorComponent;

  ngOnInit() {
    this.sortFunction = "tipo";
    let cedula = this.authService.getUser().cedula;
    this.loading = true;
    this.haskellService.getArchivos(cedula).subscribe(
      (archivos) => {
        this.archivos = archivos;
        this.loading = false;
        this.buildTreeFromList();
      },
      (error) => console.log(error)
    );

    if (this.esAlumno) {
      this.loadingCompartidos = true;
      this.haskellService.getArchivosCompartidosAlumno(cedula).subscribe(
        (archivos) => {
          this.archivosCompartidos = archivos;
          this.archivosCompartidosSinDuplicados = archivos.filter(
            (arch) =>
              arch.archivoOrigenId != -1 ||
              !archivos.some((a) => a.archivoOrigenId == arch.id)
          );
          this.loadingCompartidos = false;
        },
        (error) => console.log(error)
      );
    }
    this.ordenarArchivos();
  }

  showNotification(type: "error" | "success" | "warning", traslation: string) {
    this.translateService
      .get(traslation)
      .subscribe(this.notificationTypeDictonarty[type]);
  }

  ordenarMixto() {
    //1. primero las carpetas.
    this.archivosCompartidosSinDuplicados =
      this.archivosCompartidosSinDuplicados.sort(this.ordenarTipo);
    this.directorioActual.archivos = this.directorioActual.archivos.sort(
      this.ordenarTipo
    );

    var archs1 = this.directorioActual.archivos;
    var archs2 = this.archivosCompartidosSinDuplicados;

    var archs1_directorios = archs1.filter(function (a) {
      return a.directorio;
    });

    var archs1_archivos = archs1.filter(function (a) {
      return !a.directorio;
    });

    var archs2_directorios = archs2.filter(function (a) {
      return a.directorio;
    });

    var archs2_archivos = archs2.filter(function (a) {
      return !a.directorio;
    });
    //2. dentro de cada categoría ordeno alfabéticamente.

    archs1_archivos = archs1_archivos.sort(this.ordenarAlph);
    archs1_directorios = archs1_directorios.sort(this.ordenarAlph);
    archs2_archivos = archs2_archivos.sort(this.ordenarAlph);
    archs2_directorios = archs2_directorios.sort(this.ordenarAlph);

    for (var i in archs1_archivos) {
      archs1_directorios.push(archs1_archivos[i]);
    }
    for (var i in archs2_archivos) {
      archs2_directorios.push(archs2_archivos[i]);
    }

    this.directorioActual.archivos = archs1_directorios;
    this.archivosCompartidosSinDuplicados = archs2_directorios;
  }

  ordenarAlph(a, b) {
    if (a.nombre.toLowerCase() < b.nombre.toLowerCase()) return -1;
    if (a.nombre.toLowerCase() > b.nombre.toLowerCase()) return 1;
    return 0;
  }
  ordenarFecha(a, b) {
    if (a.fechaCreacion < b.fechaCreacion) return -1;
    if (a.fechaCreacion > b.fechaCreacion) return 1;
    return 0;
  }
  ordenarTipo(a, b) {
    if (a.directorio && !b.directorio) return -1;
    if (!a.directorio && b.directorio) return 1;
    return 0;
  }
  ordenarPorTipo() {
    this.sortFunction = "tipo";
    this.ordenarArchivos();
  }
  ordenarPorFecha() {
    this.sortFunction = "fecha";
    this.ordenarArchivos();
  }
  ordenarFechaCreacion() {
    this.archivosCompartidosSinDuplicados =
      this.archivosCompartidosSinDuplicados.sort(this.ordenarFecha);
    this.directorioActual.archivos = this.directorioActual.archivos.sort(
      this.ordenarFecha
    );
  }

  ordenarArchivos() {
    var tipo = this.sortFunction;
    if (tipo === "tipo") {
      this.ordenarMixto();
    } else if (tipo === "fecha") {
      this.ordenarFechaCreacion();
    }
  }

  mostrarEliminarDialogo() {
    // Chequear si se seleccionó un archivo
    if (!this.archivoSeleccionado) {
      this.showNotification("warning", "i18n.warning.file.noSelected");
      return;
    }

    // Si el archivo es del alumno, se puede eliminar.
    // No se controla por creador, dado que los compartidos mantienen este atributo
    if (!this.archivos.some((arch) => arch.id == this.archivoSeleccionado.id)) {
      this.showNotification("warning", "i18n.warning.file.noPermissionDelete");
      return;
    }

    this.fileNameToRemove = { fileName: this.archivoSeleccionado.nombre };

    // Determina el tipo de modal a renderizar
    this.modalTypeIsFile = !this.archivoSeleccionado.directorio;

    // Mostrar el modal
    this.modalRemoveFile = true;
    this.modalRemoveFileOpened = true;
  }

  seleccionarDirectorioAMover() {
    if (!this.archivoSeleccionado) {
      this.showNotification("warning", "i18n.warning.file.noSelected");
      return;
    }

    // Si el archivo es del alumno, se puede mover.
    // No se controla por creador, dado que los compartidos mantienen este atributo
    if (!this.archivos.some((arch) => arch.id == this.archivoSeleccionado.id)) {
      this.showNotification("warning", "i18n.warning.file.noPermissionMove");
      return;
    }

    this.navBack(false);
    this.currentDirOfFileToMove = this.directorioActual;

    // Mostrar el modal
    this.modalMoveFile = true;
    this.modalMoveFileOpened = true;
  }

  /**
   * Dado un ID de directorio, refresca la UI con el contenido del directorio
   * identificado por `idDirectorioActual`
   * @param idDirectorioActual ID del directorio sobre el cual se quiere mostrar los archivos
   */
  recargarArchivos(idDirectorioActual) {
    let cedula = this.authService.getUser().cedula;
    this.loading = true;

    this.haskellService.getArchivos(cedula).subscribe(
      (archivos) => {
        this.archivos = archivos;
        this.loading = false;
        this.buildTreeFromList_setearDirectorioActual(idDirectorioActual);
      },
      (error) => console.log(error)
    );
  }

  /**
   * Navega hacia el directorio padre en la vista principal de directorios.
   * @param shouldUpdateSelectedFile Determina si se debe actualizar el archivo seleccionado cuando se navega hacia atrás
   */
  navBack(shouldUpdateSelectedFile = true) {
    const { padreId } = this.directorioActual;

    if (padreId === ID_ROOT_DIR) {
      return;
    }

    // Actualiza el current path
    const lastDirectoryIndex = this.currentPath.lastIndexOf(
      `${this.directorioActual.nombre}`
    );

    this.currentPath = this.currentPath.substring(0, lastDirectoryIndex);

    const padre = this.archivos.filter((file) => file.id === padreId)[0];

    if (shouldUpdateSelectedFile) {
      // Cuando se selecciona un directorio cuyo id es el root, significa que ese
      // es el último archivo de la rama de directorios. En otras palabras, el
      // root se identifica porque su padreId es ID_ROOT_DIR.
      const archivoSeleccionado =
        padre.padreId == ID_ROOT_DIR ? undefined : padre;
      this.actualizarArchivoSeleccionado(archivoSeleccionado);
    }

    // Actualiza la vista de directorios y archivos
    this.directorioActual = padre;
  }

  /**
   * Localmente en la vista de directorios del modal, navega hacia el
   * directorio padre.
   * @param event Archivo sobre el cual se quiere navegar hacia su directorio padre
   */
  navigateBackModal(event: CustomEvent<Archivo>) {
    const { padreId } = event.detail;

    // No se puede navegar al padre de root
    if (padreId == ID_ROOT_DIR) {
      return;
    }

    const archivosList: Archivo[] = this.sessionService.getArchivosList();

    // Se queda con el archivo identificado por padreId
    this.currentDirOfFileToMove = archivosList.filter(
      (file) => file.id === padreId
    )[0];
  }

  setSoloLectura = function (arch: Archivo) {
    this.archivoSeleccionado.editable = !this.archivoSeleccionado.editable;
    this.haskellService
      .editarArchivo(this.archivoSeleccionado.id, this.archivoSeleccionado)
      .subscribe(
        () => {
          this.translateService
            .get("i18n.msg.file.modified")
            .subscribe((res) => {
              console.log(res);
            });
        },
        (error) => {
          this.notifService.error(error);
        }
      );
  };

  cargarArchivo() {
    if (!this.archivoSeleccionado) {
      this.showNotification("warning", "i18n.warning.file.noSelected");
      return;
    }

    if (this.archivoSeleccionado.directorio) {
      this.translateService
        .get("i18n.warning.file.noSelected")
        .subscribe((res) => this.notifService.warning(res, false));
      return;
    }

    // Si el archivo es compartido con el grupo, editabe y no lo he editado, lo voy a buscar al servidor.
    if (
      this.archivosCompartidos.some(
        (arch) => arch.id == this.archivoSeleccionado.id
      ) &&
      this.archivoSeleccionado.editable &&
      this.archivoSeleccionado.archivoOrigenId == -1
    ) {
      if (this.hayArchivoMio()) {
        this.seleccionarArchivoMio();
        this.sessionService.setArchivo(this.archivoSeleccionado);
        this.router.navigate(["/matefun"]);
      } else {
        let cedula = this.authService.getUser().cedula;
        this.haskellService
          .getCopiaArchivoCompartidoGrupo(cedula, this.archivoSeleccionado.id)
          .subscribe(
            (archivo) => {
              this.sessionService.setArchivo(archivo);
              this.router.navigate(["/matefun"]);
            },
            (error) => {
              console.log(error);
            }
          );
      }
    } else {
      this.sessionService.setArchivo(this.archivoSeleccionado);
      this.router.navigate(["/matefun"]);
    }
  }

  confirmarEntrega() {
    // Mostrar el modal
    this.modalSendFile = true;
    this.modalSendFileOpened = true;
  }

  entregarArchivo() {
    this.archivoSeleccionado.estado = "Entregado"; //this.titlecasePipe.transform(this.translateService.get('i18n.action.sent'));
    this.haskellService
      .editarArchivo(this.archivoSeleccionado.id, this.archivoSeleccionado)
      .subscribe(
        (archivo) => {
          this.archivoSeleccionado = archivo;

          // Cerrar el modal en caso de éxito
          this.modalSendFileOpened = false;
        },
        (error) => {
          this.notifService.error(error);

          // Cerrar el modal en caso de falla
          this.modalSendFileOpened = false;
        }
      );
  }

  buildTreeFromList_setearDirectorioActual(idDirectorioActual) {
    var archivos = this.archivos;
    this.sessionService.setArchivosList(archivos);
    var root: Archivo;

    for (var a in archivos) {
      var arch = archivos[a];
      if (arch.padreId === -1) {
        root = arch;
      }
    }
    this.idRecorridos = [root.id];
    var archivos2 = archivos.filter(function (a) {
      return a.id !== root.id;
    });
    var directorioActual = this.archivos.filter(function (a) {
      return a.id === idDirectorioActual;
    })[0];
    var tree = this.buildTree(archivos2, root);
    this.tree = tree;
    this.directorioActual = directorioActual;
    this.ordenarArchivos();
    this.sessionService.setArchivosTree(tree);
  }

  buildTreeFromList() {
    var archivos = this.archivos;
    this.sessionService.setArchivosList(archivos);
    var root: Archivo;

    for (var a in archivos) {
      var arch = archivos[a];
      if (arch.padreId === -1) {
        root = arch;
      }
    }
    this.idRecorridos = [root.id];
    var archivos2 = archivos.filter(function (a) {
      return a.id !== root.id;
    });
    var tree = this.buildTree(archivos2, root);
    this.tree = tree;
    this.directorioActual = tree;
    this.ordenarArchivos();
    this.sessionService.setArchivosTree(tree);
  }

  buildTree(archivos, root) {
    root.archivos = this.getArchivos(root.id, archivos);
    for (var a in root.archivos) {
      if (
        root.archivos[a].directorio &&
        this.idRecorridos[root.archivos[a].id] === undefined
      ) {
        var id = root.archivos[a].id;
        var archivos2 = archivos.filter(function (a) {
          return a.id !== id;
        });
        root.archivos[a] = this.buildTree(archivos2, root.archivos[a]);
      }
    }
    return root;
  }

  getArchivos(id, archivos) {
    return archivos.filter(function (a) {
      return a.padreId === id;
    });
  }

  cantArchivos(idPadre, archivos) {
    return archivos.filter(function (a) {
      a.padreId === idPadre;
    }).length;
  }

  elem(id, archivos) {
    if (archivos === []) {
      return false;
    } else {
      return (
        archivos.filter(function (a) {
          a.id === id;
        }).length > 0
      );
    }
  }

  /**
   * Renderiza en pantalla el modal de agregar archivos o directorios,
   * dependiendo del valor de `modalTypeIsFile`.
   * @param modalTypeIsFile Con `true` se indica que el modal a renderizar es el de agregar archivo. De otro modo, se renderiza el de agregar directorio.
   */
  mkFile(modalTypeIsFile: boolean) {
    this.modalTypeIsFile = modalTypeIsFile;

    // Mostrar el modal
    this.modalCreateFile = true;
    this.modalCreateFileOpened = true;
  }

  /**
   * Valida y confirma la creación del archivo.
   * @param event Evento devuelto por el modal asociado para crear el archivo. En el detalle contiene el `nombre` y `descripcion` del archivo que se desea agregar.
   */
  confirmFileCreation(event: CustomEvent) {
    const { nombre, descripcion } = event.detail;

    // Antes que nada, se chequea que empiece con mayúscula
    if (!STARTS_WITH_CAPITAL_LETTER_REGEX.test(nombre)) {
      this.showNotification("warning", "i18n.warning.file.capitalLetter");
      return;
    }

    const archivo = new Archivo();
    archivo.cedulaCreador = this.directorioActual.cedulaCreador;
    archivo.contenido = this.modalTypeIsFile ? "" : descripcion || "";
    archivo.directorio = !this.modalTypeIsFile;
    archivo.editable = true;
    archivo.fechaCreacion = new Date();
    archivo.nombre = nombre;
    archivo.padreId = this.directorioActual.id;

    const that = this;
    const idDirectorioActual = this.directorioActual.id;

    this.haskellService.crearArchivo(archivo).subscribe(
      () => {
        that.recargarArchivos(idDirectorioActual);

        // Cerrar el modal en caso de éxito
        that.modalCreateFileOpened = false;
      },
      (error) => {
        that.notifService.error(error.text());

        // Cerrar el modal en caso de error
        that.modalCreateFileOpened = false;
      }
    );
  }

  /**
   * Valida y confirma la operación de mover el archivo/carpeta.
   * @param event Evento devuelto por el modal asociado para mover el archivo. El detalle contiene el `idPadre` del archivo que se desea mover.
   */
  confirmFileMove(event: CustomEvent) {
    if (!this.archivoSeleccionado) {
      this.showNotification("warning", "i18n.warning.file.noSelected");
      return;
    }

    const nuevoDirectorioPadreId = event.detail.padreId;

    // Se actualiza en el cliente el directorio padre del archivo a mover
    this.archivoSeleccionado.padreId = nuevoDirectorioPadreId;

    if (this.archivoSeleccionado.directorio) {
      delete this.archivoSeleccionado["archivos"];
    }

    this.haskellService
      .editarArchivo(this.archivoSeleccionado.id, this.archivoSeleccionado)
      .subscribe(
        () => {
          this.recargarArchivos(this.directorioActual.id);

          // Dado que se cambia la ubicación del directorio actual, hay que
          // rearmar el currentPath
          if (this.archivoSeleccionado.directorio) {
            this.inicializarCurrentPath(this.directorioActual);
          }

          this.archivoSeleccionado = null;

          // Cerrar el modal en caso de éxito
          this.modalMoveFileOpened = false;
        },
        (error) => {
          this.notifService.error(error.text());

          // Cerrar el modal en caso de error
          this.modalMoveFileOpened = false;
        }
      );
  }

  /**
   * Valida y confirma la operación de borrar el archivo/carpeta.
   */
  confirmFileDeletion() {
    const directorio = this.archivoSeleccionado.directorio;

    this.haskellService.eliminarArchivo(this.archivoSeleccionado.id).subscribe(
      () => {
        let idDirActual;

        this.archivoSeleccionado.eliminado = true;

        // Si el archivo seleccionado es un directorio, se borran todos los
        // archivos del directorio
        if (directorio) {
          delete this.archivoSeleccionado["archivos"];

          // Actualiza el current path
          const lastDirectoryIndex = this.currentPath.lastIndexOf(
            `${this.directorioActual.nombre}`
          );
          this.currentPath = this.currentPath.substring(0, lastDirectoryIndex);

          idDirActual = this.directorioActual.padreId;

          // Si se borra el directorio actual, se selecciona el directorio
          // padre en caso de que este no sea el root
          this.archivoSeleccionado =
            idDirActual !== ID_ROOT_DIR ? idDirActual : null;
        } else {
          idDirActual = this.directorioActual.id;
          this.archivoSeleccionado = null;
        }

        this.recargarArchivos(idDirActual);

        // Cerrar el modal en caso de éxito
        this.modalRemoveFileOpened = false;
      },
      (error) => {
        this.notifService.error(error);

        // Cerrar el modal en caso de error
        this.modalRemoveFileOpened = false;
      }
    );
  }

  confirmFileShare(event: CustomEvent<Grupo>) {
    const selectedGroup = event.detail;

    if (!selectedGroup) {
      this.showNotification("error", "i18n.warning.group.select");
      return;
    }

    this.haskellService
      .compartirArchivoGrupo(selectedGroup, this.archivoSeleccionado.id)
      .subscribe(
        () => {
          this.showNotification("success", "i18n.msg.file.shared");

          // Cerrar el modal en caso de éxito
          this.modalShareFileOpened = false;
        },
        (error) => {
          this.notifService.error(error);

          // Cerrar el modal en caso de error
          this.modalShareFileOpened = false;
        }
      );
  }

  /**
   * Dado el directorio actual, se contruye el path absoluto (se actualiza
   * currentPath) que identifica al directorio. Esta función es útil cuando un
   * directorio es movido de lugar y se desconoce la nueva ubicación del
   * directorio.
   * @param lastDirectory Directorio sobre el cual se quiere obtener el path
   */
  inicializarCurrentPath(lastDirectory: Archivo) {
    // Si el directorio está en root, el currentPath es "/"
    if (lastDirectory.padreId === ID_ROOT_DIR) {
      this.currentPath = "/";
      return;
    }

    let path = "/";
    let currentDirectory = lastDirectory;

    const archivosList: Archivo[] = this.sessionService.getArchivosList();

    // Se recorren los ancestros de currentDirectory
    while (currentDirectory.padreId !== ID_ROOT_DIR) {
      path = "/" + currentDirectory.nombre + path;

      // Se queda con el archivo identificado por padreId
      currentDirectory = archivosList.filter(
        (file) => file.id === currentDirectory.padreId
      )[0];
    }

    this.currentPath = path;
  }

  seleccionarArchivo(archivo: Archivo) {
    if (archivo.directorio) {
      this.directorioActual = archivo;

      // Actualizar el currentPath para así siempre lo puede recibir el modal de
      // mover archivo
      this.currentPath += `${archivo.nombre}/`;
    } else {
      this.sessionService.setDirectorioActual(this.directorioActual);
      this.sessionService.setArchivosCompartidos(this.archivosCompartidos);
      this.sessionService.cargarDependencias(archivo);
    }

    this.actualizarArchivoSeleccionado(archivo);

    //this.ordenarArchivos();
  }

  /**
   * Actualiza el estado del archivo seleccionado, así como la preview del
   * contenido de dicho archivo.
   * @param archivo Archivo a seleccionar
   */
  actualizarArchivoSeleccionado(archivo: Archivo) {
    this.archivoSeleccionado = archivo;
    this.preview = archivo ? archivo.contenido : "";
  }

  compartirArchivo() {
    if (!this.archivoSeleccionado) {
      this.showNotification("warning", "i18n.warning.file.noSelected");
      return;
    }

    const grupos = this.sessionService.getGrupos();
    this.fileNameToShare = { fileName: this.archivoSeleccionado.nombre };

    if (grupos == undefined) {
      this.haskellService
        .getGrupos(this.authService.getUser().cedula)
        .subscribe((gruposRest) => {
          // Se guardan los grupos en la sesión
          this.sessionService.setGrupos(gruposRest);

          this.groupsToShareFile = gruposRest;

          // Mostrar el modal
          this.modalShareFile = true;
          this.modalShareFileOpened = true;
        });
    } else {
      this.groupsToShareFile = grupos;

      // Mostrar el modal
      this.modalShareFile = true;
      this.modalShareFileOpened = true;
    }
  }

  hayArchivoOriginal() {
    return (
      !this.archivoSeleccionado.directorio &&
      this.archivosCompartidos.some(
        (a) => a.id == this.archivoSeleccionado.archivoOrigenId
      )
    );
  }

  seleccionarArchivoOriginal() {
    this.archivoSeleccionado = this.archivosCompartidos.find(
      (arch) => arch.id == this.archivoSeleccionado.archivoOrigenId
    );
    this.preview = this.archivoSeleccionado.contenido;
  }

  hayArchivoMio() {
    return (
      !this.archivoSeleccionado.directorio &&
      this.archivosCompartidos.some(
        (a) => a.archivoOrigenId == this.archivoSeleccionado.id
      )
    );
  }

  seleccionarArchivoMio() {
    this.archivoSeleccionado = this.archivosCompartidos.find(
      (a) => a.archivoOrigenId == this.archivoSeleccionado.id
    );
    this.preview = this.archivoSeleccionado.contenido;
  }

  verCalificacion() {
    if (!this.archivoSeleccionado) {
      return;
    }

    // Mostrar el modal
    this.modalSeeScore = true;
    this.modalSeeScoreOpened = true;
  }
}
