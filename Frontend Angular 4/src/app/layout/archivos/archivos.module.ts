import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { ArchivosComponent } from "./archivos.component";
import { CommonModule } from "@angular/common";
import { ArchivosRoutingModule } from "./archivos-routing.module";
import { FilterPipe } from "../../shared/pipes/filter.pipe";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { LtCodemirrorModule } from "lt-codemirror";
import { NotificacionModule } from "../../notificacion/notificacion.module";
import { I18nModule } from "../../shared/modules/translate/i18n.module";
import { TitleCaseModule } from "../../shared/modules/titlecase.module";

@NgModule({
  imports: [
    CommonModule,
    ArchivosRoutingModule,
    FormsModule,
    NgbModule,
    LtCodemirrorModule,
    NotificacionModule,
    I18nModule,
    TitleCaseModule,
  ],
  declarations: [ArchivosComponent, FilterPipe],
  exports: [ArchivosComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class ArchivosModule {}
