import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { GHCIService } from "../shared/services/ghci.service";
import { NotificacionService } from "../shared/services/notificacion.service";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-layout",
  templateUrl: "./layout.component.html",
  styleUrls: ["./layout.component.scss"],
  providers: [GHCIService],
})
export class LayoutComponent implements OnInit {
  translateService: any;

  constructor(public router: Router, public translate: TranslateService) {
    this.translateService = translate;
  }
  ngOnInit() {
    if (this.router.url === "/") {
      this.translateService
        .get("i18n.code")
        .subscribe((res) => this.router.navigate(["/" + res + "/login"]));
    }
  }
}
