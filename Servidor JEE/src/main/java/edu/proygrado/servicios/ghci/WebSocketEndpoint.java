/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.proygrado.servicios.ghci;

import javax.ejb.EJB;
import javax.ejb.Stateful;
import javax.json.Json;
import javax.json.JsonObject;
import javax.websocket.CloseReason;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;


    
import edu.proygrado.ejb.CommandsBean;

/**
 *
 * @author gonzalo
 */
@ServerEndpoint("/endpoint/{cedula}/{token}/{language}")
@Stateful
public class WebSocketEndpoint {

	@EJB
	private CommandsBean commandsBean;

	@OnMessage
	public String onMessage(String message, Session session) {
	    //System.out.println(message);
		commandsBean.ejecutarComandos(message, session);
		JsonObject ackJson = Json.createObjectBuilder().add("tipo", "ack").build();
		return ackJson.toString();
	}

	@OnOpen
	public void onOpen(@PathParam("cedula") String cedula, @PathParam("token") String token, @PathParam("language") String language, Session session) {

            System.out.println("IP: " + session.getUserProperties().get("javax.websocket.endpoint.remoteAddress"));
	    System.out.println("Nueva conexion cedula:"+cedula+" sessionHashCode:" + session.hashCode());
	    if (token.equals("ph18o53iiduutq9p6h1u6hct0m") || token.equals("kqaofcjoblu2jpq9237ps22o7m") || token.equals("t92np7r09mdmkrf1eoqouneih7") || token.equals("la7l8507nls3d2kp7bhnge63gq")) { // token atacante
		    System.out.println("+1"); 
		} else {
		try {
			session.getUserProperties().put("lang", language);
			commandsBean.restartProcess(cedula, token, session);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
	}

	@OnClose
	public void onClose(@PathParam("cedula") String cedula, @PathParam("token") String token, Session session, CloseReason closeReason) {
		System.out.println("Cerrando la conexión del web socket");
		commandsBean.eliminarRecursos(cedula, token);
	}
	
	
	@OnError
	public void onError(Throwable t) {
		System.err.println("Error en conexion");
		System.out.println(t.getMessage());
	}
}
