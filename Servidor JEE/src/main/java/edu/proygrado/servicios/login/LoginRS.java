package edu.proygrado.servicios.login;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.PathParam;
import javax.ws.rs.HeaderParam;

import edu.proygrado.dto.CredencialesDTO;
import edu.proygrado.dto.UsuarioDTO;
import edu.proygrado.ejb.LoginEJB;
import edu.proygrado.matefun.MatefunException;
import edu.proygrado.matefun.MatefunUnauthorizedException;

@Stateless
@Path("/login")
public class LoginRS{

	@EJB
    private LoginEJB loginEJB;
	
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UsuarioDTO login(CredencialesDTO credenciales) throws MatefunException{
    	UsuarioDTO user=  loginEJB.login(credenciales.getCedula(), credenciales.getPassword());
        return user;
    }

    @POST
    @Path("{cedula}/logout")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public String logout(@PathParam("cedula") String cedula, @HeaderParam("authorization") String auth) throws MatefunException{
	String token = auth.substring("Bearer ".length()).trim();
    	return loginEJB.logout(cedula,token);
 
    }


    @GET
    @Path("/datosDePrueba")
    public String cargarDatosDePrueba(){
    	return loginEJB.cargarDatosDePrueba();
    }
    
    

}
