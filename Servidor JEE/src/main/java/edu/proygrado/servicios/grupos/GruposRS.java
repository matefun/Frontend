package edu.proygrado.servicios.grupos;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.servlet.http.HttpServletRequest;
import javax.inject.Inject;

import edu.proygrado.dto.ArchivoDTO;
import edu.proygrado.dto.GrupoDTO;
import edu.proygrado.ejb.GruposEJB;
import edu.proygrado.modelo.GrupoPK;
import edu.proygrado.modelo.LiceoPK;
import edu.proygrado.ejb.InvitadoEJB;
import edu.proygrado.matefun.MatefunException;
import edu.proygrado.modelo.Usuario;
import edu.proygrado.modelo.Docente;

@Stateless
@Path("/grupo")
public class GruposRS {
    @EJB
    private GruposEJB gruposEJB;

    @EJB
    private InvitadoEJB invitadoEJB;
    
    @Inject
    private HttpServletRequest httpServletRequest;

    
	@POST
	@Path("/{liceoId}/{anio}/{grado}/{grupo}/archivo")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public void asignarArchivo(@PathParam("liceoId") Long liceoId, @PathParam("anio") Integer anio,
				   @PathParam("grado") Integer grado, @PathParam("grupo") String grupo, ArchivoDTO archivoDTO) throws Exception{
          if(esDocente()){
	        LiceoPK lpk = new LiceoPK(liceoId);
		GrupoPK grupoPK = new GrupoPK(anio, grado, grupo, lpk);
		gruposEJB.agregarArchivoGrupo(archivoDTO.getId(), grupoPK);
	  }else{
	      throw new MatefunException("Usuario no autorizado");
	  }

	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<GrupoDTO> getGruposDocente(@QueryParam("cedula") String cedula) throws Exception{
          if(esDocente()){
		return this.gruposEJB.getGruposDocente(cedula);
	  }else{
	      throw new MatefunException("Usuario no autorizado");
	  }
	}
	

	@POST
	@Path("{cedula}/add")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String agregarUsuarioGrupo(@PathParam("cedula") String cedula, GrupoDTO grupoDTO) throws Exception {
          if(esDocente()){
		return this.gruposEJB.agregarUsuarioGrupo(cedula,grupoDTO);
	  }else{
	      throw new MatefunException("Usuario no autorizado");
	  }
	    

	}

    
    private boolean esDocente() throws MatefunException{
    	String token = getToken();
    	Usuario usuario = invitadoEJB.getUsuario(token);
    	if(usuario!=null && usuario instanceof Docente){
    		return true;
    	}
    	return false;
    }

    
    
    private String getToken() throws MatefunException{
    	String token = httpServletRequest.getHeader(HttpHeaders.AUTHORIZATION);
    	if(token!=null && token.contains("Bearer ")){
    		token = token.substring("Bearer ".length());
    	}else{
    		throw new MatefunException("Usuario no autorizado");
    	}
    	return token;
    }

    
}
