package edu.proygrado.servicios.usuario;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import edu.proygrado.dto.BackupAlumnoDTO;
import edu.proygrado.dto.BackupDocenteDTO;
import edu.proygrado.dto.BackupUsuarioDTO;
import edu.proygrado.dto.ConfiguracionDTO;
import edu.proygrado.ejb.UsuarioEJB;
import edu.proygrado.ejb.InvitadoEJB;
import edu.proygrado.matefun.MatefunException;
import edu.proygrado.modelo.Usuario;
import edu.proygrado.modelo.Docente;

@Stateless
@Path("/usuario")
public class UsuarioRS {
	@EJB
    private UsuarioEJB usuarioEJB;

    @EJB
    private InvitadoEJB invitadoEJB;
    
    @Inject
    private HttpServletRequest httpServletRequest;

    
	@PUT
	@Path("{cedula}/configuracion")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ConfiguracionDTO actualizarConfiguracion(@PathParam("cedula") String cedula, ConfiguracionDTO configuracion) throws Exception {
        if(!(esDocente() || esUsuario(cedula))){
        	return configuracion;
        }else{
        	return usuarioEJB.actualizarConfiguracion(cedula,configuracion);
        }
    }
	
	@GET
	@Path("{cedula}/backup")
	@Produces(MediaType.APPLICATION_JSON)
	public BackupUsuarioDTO backupUsuario(@PathParam("cedula") String cedula) throws MatefunException{
          if(esDocente() || esUsuario(cedula)){
		return usuarioEJB.respaldarUsuario(cedula);
	  }else{
	      throw new MatefunException("Usuario no autorizado");
	  }
	}
	
	@POST
	@Path("alumno/restore")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String restaurarAlumno(BackupAlumnoDTO backupAlumnoDTO) throws MatefunException{
          if(esDocente()){
		return usuarioEJB.restaurarAlumno(backupAlumnoDTO);
	  }else{
	      throw new MatefunException("Usuario no autorizado");
	  }
	}
	
	@POST
	@Path("docente/restore")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String restaurarDocente(BackupDocenteDTO backupDocenteDTO) throws MatefunException{
          if(esDocente()){
		return usuarioEJB.restaurarDocente(backupDocenteDTO);
	  }else{
	      throw new MatefunException("Usuario no autorizado");
	  }
		
	}


	@DELETE
	@Path("{cedula}")
	@Produces(MediaType.TEXT_PLAIN)
	public String eliminarUsuario(@PathParam("cedula") String cedula) throws MatefunException {
          if(esDocente()){
		return usuarioEJB.eliminarUsuario(cedula);
	  }else{
	      throw new MatefunException("Usuario no autorizado");
	  }
		
	}

    
    private boolean esDocente() throws MatefunException{
    	String token = getToken();
    	Usuario usuario = invitadoEJB.getUsuario(token);
    	if(usuario!=null && usuario instanceof Docente){
    		return true;
    	}
    	return false;
    }

    
    private boolean esUsuario(String cedula) throws MatefunException{
    	String token = getToken();
    	Usuario usuario = invitadoEJB.getUsuario(token);
    	if(usuario!=null && usuario.getCedula().equals(cedula)){
    		return true;
    	}
    	return false;
    }
    
    private String getToken() throws MatefunException{
    	String token = httpServletRequest.getHeader(HttpHeaders.AUTHORIZATION);
    	if(token!=null && token.contains("Bearer ")){
    		token = token.substring("Bearer ".length());
    	}else{
    		throw new MatefunException("Usuario no autorizado");
    	}
    	return token;
    }

}
