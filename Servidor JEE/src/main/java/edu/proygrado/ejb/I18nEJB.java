package edu.proygrado.ejb;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.Reader;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.PostConstruct;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.LockType;
import javax.ejb.Lock;
import javax.inject.Inject;

import org.apache.commons.text.StringEscapeUtils;

@Singleton
@Startup
@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)
public class I18nEJB {


    private Map<String, Map<String, String>> languages;
	
	@PostConstruct
    private void Initialize(){
		languages = new HashMap<String, Map<String, String>>();
    }
    	
	@Lock(LockType.READ)
	public Map<String, String> getLanguage(String path, String lang) {
		if (!this.languages.containsKey(lang)) {
			try {
				String filePath = String.format("%s/%s.po", path, lang);
				this.languages.put(lang, LoadPoFile(filePath));
			} catch (FileNotFoundException e) {
				System.out.println("File not found exception getLanguage for " + lang + " force loading default: en");
				
				if (!this.languages.containsKey("en")) {
					try {
						String filePath = String.format("%s/%s.po", path, "en");
						this.languages.put("en", LoadPoFile(filePath));
						
						return this.languages.get("en");
					} catch (FileNotFoundException e2) {
						System.out.println("File not found exception getLanguage for default");
					} 
				}				
			} 
		}
		
		return this.languages.get(lang);
	}

	private Map<String, String> LoadPoFile(String path) throws FileNotFoundException {

		Pattern LINE_PATTERN = Pattern.compile("^([\\w_\\[\\]]*)\\s*\\\"(.*)\\\"$");
		
		Map<String, String> resources = new HashMap<String, String>();
		File file = new File(path);

		LineNumberReader reader = new LineNumberReader(new FileReader(file));

		if (reader != null) {
			String line = null;
			String key = null;
			String value = null;

			try {

				while ((line = reader.readLine()) != null) {

					if (line.startsWith("#")) {

						// Parsing PO file, comment skipped

					} else if (line.trim().length() == 0) {

						// Parsing PO file, whitespace line skipped

					} else {
						Matcher matcher = LINE_PATTERN.matcher(line);

						if (matcher.matches()) {
							String type = matcher.group(1);
							String str = matcher.group(2);

							if ("msgid".equals(type)) {

								if ( key != null && value != null ) {
									// Parsing PO file, key,value pair found [" + key + " => " + value + "]");
									resources.put(StringEscapeUtils.unescapeJava(key), StringEscapeUtils.unescapeJava(value));
									key = null;
									value = null;
								}	
								
								key = str;
								// Parsing PO file, msgid found [" + key + "]");
							}
							else if ("msgstr".equals(type)) {
								value = str;
								// Parsing PO file, msgstr found [" + value	+ "]");								
							}
							else if ( type == null || type.length()==0 ) {
								if ( value == null ) {
									// Parsing PO file, addition to msgid found	
									key += str;
								}
								else {
									// Parsing PO file, addition to msgstr found 
									value += str;									
								}	
							}
						} else {

							// Parsing PO file, invalid syntax 
						}
					}
				}

				if ( key != null && value != null ) {
					// Parsing PO file, key,value pair found [" + key + " => " + value + "]");					
					resources.put(StringEscapeUtils.unescapeJava(key), StringEscapeUtils.unescapeJava(value));
					key = null;
					value = null;
				}

				reader.close();
			} catch (IOException e) {	

			}
		}
		
		return resources;

	}

}

  
	