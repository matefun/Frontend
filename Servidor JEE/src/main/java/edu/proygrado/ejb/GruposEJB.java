package edu.proygrado.ejb;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import edu.proygrado.matefun.MatefunException;
import edu.proygrado.dto.GrupoDTO;
import edu.proygrado.modelo.Archivo;
import edu.proygrado.modelo.Grupo;
import edu.proygrado.modelo.GrupoPK;
import edu.proygrado.modelo.Liceo;
import edu.proygrado.modelo.LiceoPK;
import edu.proygrado.modelo.Alumno;
import edu.proygrado.modelo.Docente;
import edu.proygrado.modelo.Usuario;



@Stateless
public class GruposEJB {

	@PersistenceContext(unitName = "matefunDS")
	private EntityManager em;

        public String agregarUsuarioGrupo(String cedula, GrupoDTO grupoDTO) throws Exception {
		Usuario usuario = em.find(Usuario.class, cedula);
		if(usuario!=null){
        		LiceoPK lpk = new LiceoPK(grupoDTO.getLiceoId());
			GrupoPK gpk = new GrupoPK(grupoDTO.getAnio(), grupoDTO.getGrado(), grupoDTO.getGrupo(), lpk);
			Grupo grupo = em.find(Grupo.class, gpk);
			if (grupo==null){
			    System.out.println("creando grupo");
			    grupo = new Grupo(gpk);
			    em.persist(grupo);
			}
			if (usuario instanceof  Alumno) {
			    grupo.addAlumno((Alumno) usuario);
			}
			else {
			    ((Docente)usuario).addGrupoAsignado(grupo);
			}
		}
		else{

			throw new MatefunException("No existe el usuario de cedula "+cedula);
		}
		return ("Usuario " + cedula + "agregado a grupo.");
	}
    
	public void agregarArchivoGrupo(long archivoId, GrupoPK grupoPK) throws Exception {
		Grupo grupo = em.find(Grupo.class, grupoPK);
		if (grupo == null) {
			throw new Exception(
					"No existe el grupo " + grupoPK.getAnio() + " " + grupoPK.getGrado() + grupoPK.getGrupo());
		}
		Archivo archivo = em.find(Archivo.class, archivoId);
		if (archivo == null) {
			throw new Exception("No existe el archivo" + archivoId);
		}
		if (!grupo.getArchivos().contains(archivo)) {
			grupo.addArchivo(archivo);
		}
	}
	
	
	public List<GrupoDTO> getGruposDocente(String cedulaDocente){
		List<Grupo> grupos = em.createQuery("select ga from Docente d join d.gruposAsignados ga where d.cedula =:cedulaDocente")
				.setParameter("cedulaDocente", cedulaDocente)
				.getResultList();
		List<GrupoDTO> gruposDTO = new ArrayList<>();
		for(Grupo g: grupos){
			gruposDTO.add(new GrupoDTO(g));
		}
		return gruposDTO;
	}
}
