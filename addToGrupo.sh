#! /bin/bash

# csv: user,liceo,anio,grupo,grado
# todas las filas tienen que terminar con enter
INPUT=grupos-referentes.csv

SERVER=https://www.fing.edu.uy/proyectos/matefun/servicios
#http://localhost:8080/servicios 

# parametros del script: usuario y pass
read USER # =  $1
stty -echo
read PASS # =  $2
stty echo



token=$(curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"cedula":"'$USER'","password":"'$PASS'"}' \
  "$SERVER/login" | \
    python3 -c "import sys, json; print(json.load(sys.stdin)['token'])") 

while IFS=, read -r user liceo anio grupo grado; do
    echo "user: $user"
    echo "token: $token"
    data='{"anio":"'$anio'","grupo":"'$grupo'","grado":"'$grado'","liceoId":"'$liceo'"}'
    echo $data
    curl -H "Content-Type: application/json" -H "Authorization: Bearer $token" \
         --request POST \
         --data "$data" \
         "$SERVER/grupo/$user/add" 
    echo "\n"
done < $INPUT

curl -H "Content-Type: application/json" -H "Authorization: Bearer $token" \
  --request POST \
  "$SERVER/login/$USER/logout" 


